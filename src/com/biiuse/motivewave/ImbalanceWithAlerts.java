package com.biiuse.motivewave;

import com.motivewave.platform.sdk.common.*;
import com.motivewave.platform.sdk.order_mgmt.Order;
import com.motivewave.platform.sdk.order_mgmt.OrderContext;
import com.motivewave.platform.sdk.study.StudyHeader;

import java.util.logging.Level;
import java.util.logging.Logger;

@StudyHeader(
        namespace="com.biiuse",
        rb="com.motivewave.platform.study.nls.strings",
        id="Imbalance With Alerts v2.0",
        name="Imbalance With Alerts v2.0",
        desc="",
        menu="Donovan Ward",
        overlay = true,
        signals = true,
        studyOverlay = true,
        requiresBarUpdates = true
)
public class ImbalanceWithAlerts extends VolumeImprint {


    @Override
    public void onBarOpen(DataContext ctx) {
        // Reset previous bars to 0
        for (int i = ctx.getDataSeries().size()-1; i > Math.max(0,ctx.getDataSeries().size()-6); i--) {
            ctx.getDataSeries().setDouble(i,Signals.Imbalance1Support,0d);
            ctx.getDataSeries().setDouble(i,Signals.Imbalance1Resistance,0d);
            ctx.getDataSeries().setDouble(i,Signals.Imbalance2Support,0d);
            ctx.getDataSeries().setDouble(i,Signals.Imbalance2Resistance,0d);
            ctx.getDataSeries().setDouble(i,Signals.Imbalance3Support,0d);
            ctx.getDataSeries().setDouble(i,Signals.Imbalance3Resistance,0d);
            ctx.getDataSeries().setDouble(i,Signals.AbsorptionBid,0d);
            ctx.getDataSeries().setDouble(i,Signals.AbsorptionAsk,0d);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance1Support,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance1Resistance,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance2Support,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance2Resistance,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance3Support,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.Imbalance3Resistance,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.AbsorptionBid,false);
            ctx.getDataSeries().setSignalTriggered(i,Signals.AbsorptionAsk,false);
        }
        SupportBSPPs.clear();
        ResistanceBSPPs.clear();
        AbsorptionAskBSPPs.clear();
        AbsorptionBidBSPPs.clear();
        super.onBarOpen(ctx);
    }



    @Override
    public void onTick(DataContext ctx, Tick tick) {
        super.onTick(ctx,tick);
        Settings s = getSettings();
        int last = ctx.getDataSeries().size()-1;

        // Reset all to 0
        ctx.getDataSeries().setDouble(last,Signals.Imbalance1Support,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.Imbalance1Resistance,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.Imbalance2Support,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.Imbalance2Resistance,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.Imbalance3Support,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.Imbalance3Resistance,Double.NaN);
        ctx.getDataSeries().setDouble(last,Signals.AbsorptionBid,0d);
        ctx.getDataSeries().setDouble(last,Signals.AbsorptionAsk,0d);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance1Support,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance1Resistance,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance2Support,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance2Resistance,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance3Support,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.Imbalance3Resistance,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.AbsorptionBid,false);
        ctx.getDataSeries().setSignalTriggered(last,Signals.AbsorptionAsk,false);

        long lastBarTime = ctx.getDataSeries().getStartTime(last);
        for (int i = 0; i < SupportBSPPs.size(); i ++) {
            BSPP bspp = SupportBSPPs.get(i);
            if (bspp.imbLevel == 0 && !s.getBoolean(IMBALANCE_1_SUPPORT_SIGNAL))
                continue;
            else if (bspp.imbLevel == 1 && !s.getBoolean(IMBALANCE_2_SUPPORT_SIGNAL))
                continue;
            else if (bspp.imbLevel == 2 && !s.getBoolean(IMBALANCE_3_SUPPORT_SIGNAL))
                continue;

            if (ctx.getDataSeries().getStartTime(ctx.getDataSeries().findIndex(bspp.time)) < lastBarTime) {
                SupportBSPPs.remove(bspp);
                continue;
            }

            if (!bspp.signalled) {

                Object signal = bspp.imbLevel == 0 ? Signals.Imbalance1Support :
                        bspp.imbLevel == 1 ? Signals.Imbalance2Support :
                                Signals.Imbalance3Support;

                ctx.getDataSeries().setDouble(ctx.getDataSeries().findIndex(tick.getTime()),signal,1d);
                ctx.getDataSeries().setSignalTriggered(ctx.getDataSeries().size()-1,signal,false);
                ctx.signal(ctx.getDataSeries().size()-1, signal,
                        bspp.imbLevel == 0 ? "Support 1" :
                                bspp.imbLevel == 1 ? "Support 2" :
                                        "Support 3", bspp.price);


                bspp.signalled = true;
            }
        }


        for (int i = 0; i < ResistanceBSPPs.size(); i ++) {
            BSPP bspp = ResistanceBSPPs.get(i);
            if (bspp.imbLevel == 0 && !s.getBoolean(IMBALANCE_1_RESISTANCE_SIGNAL))
                continue;
            else if (bspp.imbLevel == 1 && !s.getBoolean(IMBALANCE_2_RESISTANCE_SIGNAL))
                continue;
            else if (bspp.imbLevel == 2 && !s.getBoolean(IMBALANCE_3_RESISTANCE_SIGNAL))
                continue;

            if (ctx.getDataSeries().getStartTime(ctx.getDataSeries().findIndex(bspp.time)) < lastBarTime) {
                ResistanceBSPPs.remove(bspp);
                continue;
            }
            if (!bspp.signalled) {

                Object signal = bspp.imbLevel == 0 ? Signals.Imbalance1Resistance :
                        bspp.imbLevel == 1 ? Signals.Imbalance2Resistance :
                                Signals.Imbalance3Resistance;
                ctx.getDataSeries().setDouble(ctx.getDataSeries().findIndex(tick.getTime()),signal,1d);
                ctx.getDataSeries().setSignalTriggered(ctx.getDataSeries().size()-1,signal,false);

                ctx.signal(ctx.getDataSeries().size()-1,
                        signal,
                        bspp.imbLevel == 0 ? "Resistance 1" :
                                bspp.imbLevel == 1 ? "Resistance 2" :
                                        "Resistance 3", bspp.price
                );

                bspp.signalled = true;
            }
        }

        for (int i = 0; i < AbsorptionBidBSPPs.size(); i ++) {
            BSPP bspp = AbsorptionBidBSPPs.get(i);
            if (!s.getBoolean(ABSORPTION_BID_SIGNAL))
                continue;

            if (ctx.getDataSeries().getStartTime(ctx.getDataSeries().findIndex(bspp.time)) < lastBarTime) {
                AbsorptionBidBSPPs.remove(bspp);
                continue;
            }
            if (!bspp.signalled) {
                Object signal = Signals.AbsorptionBid;
                ctx.getDataSeries().setDouble(ctx.getDataSeries().findIndex(tick.getTime()),signal,1d);
                ctx.getDataSeries().setSignalTriggered(ctx.getDataSeries().size()-1,signal,false);

                ctx.signal(ctx.getDataSeries().size()-1,
                        signal, "Bid Absorption", bspp.price);
                bspp.signalled = true;
            }
        }
        for (int i = 0; i < AbsorptionAskBSPPs.size(); i ++) {
            BSPP bspp = AbsorptionAskBSPPs.get(i);
            if (!s.getBoolean(ABSORPTION_ASK_SIGNAL))
                continue;

            if (ctx.getDataSeries().getStartTime(ctx.getDataSeries().findIndex(bspp.time)) < lastBarTime) {
                AbsorptionAskBSPPs.remove(bspp);
                continue;
            }

            if (!bspp.signalled) {
                Object signal = Signals.AbsorptionAsk;
                ctx.getDataSeries().setDouble(ctx.getDataSeries().findIndex(tick.getTime()),signal,1d);
                ctx.getDataSeries().setSignalTriggered(ctx.getDataSeries().size()-1,signal,false);

                ctx.signal(ctx.getDataSeries().size()-1,
                        signal, "Ask Absorption", bspp.price);
                bspp.signalled = true;

            }
        }

    }

    @Override
    public void onBarUpdate(DataContext ctx) {
    }
}

