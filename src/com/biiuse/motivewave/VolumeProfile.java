package com.biiuse.motivewave;


import java.util.List;

import com.motivewave.platform.sdk.common.Instrument;
import com.motivewave.platform.sdk.common.Tick;
import com.motivewave.platform.sdk.common.Util;

public class VolumeProfile
{
  public VolumeProfile(long startTime, long endTime, Instrument instr, int rangeTicks)
  {
    this.startTime = startTime;
    this.endTime = endTime;
    this.instrument = instr;
    this.rangeTicks = rangeTicks;
    this.tickSizeD = instrument.getTickSize();
    this.tickSize = (float)instrument.getTickSize();
  }

  public boolean isSummary() { return false; }

  public VolumeProfile getPrev() { return prev; }
  public void setPrev(VolumeProfile vp) { prev = vp; }

  public VolumeProfile getNext() { return next; }
  public void setNext(VolumeProfile vp) { next = vp; }

  public double getAVAP()
  {
    int rc = rows.size();
    return rc == 0 ? 0 : totalVolume/rc;
  }

  public double getDeltaPer()
  {
    if (totalVolume <= 0) return 0;
    return totalDelta / totalVolume;
  }

  public double getDeltaChange()
  {
    if (prev == null) return 0;
    return totalDelta - prev.totalDelta;
  }

  public double getMaxDelta()
  {
    if (maxDelta == Double.MIN_VALUE) return 0;
    return maxDelta;
  }

  public double getMinDelta()
  {
    if (minDelta == Double.MAX_VALUE) return 0;
    return minDelta;
  }

  public double getMaxRowDelta()
  {
    if (maxRowDelta == Double.MIN_VALUE) calcMinMaxRowDelta();
    return maxRowDelta;
  }

  public double getMinRowDelta()
  {
    if (minRowDelta == Double.MAX_VALUE) calcMinMaxRowDelta();
    return minRowDelta;
  }

  private void calcMinMaxRowDelta()
  {
    double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
    for(var r : rows.get()) {
      double d = r.getDelta();
      if (d > max) max = d;
      if (d < min) min = d;
    }
    minRowDelta = min;
    maxRowDelta = max;
  }

  public double getMaxAskVolume()
  {
    if (maxAskVolume != 0) return maxAskVolume;
    double m = 0;
    for(var row : rows.get()) {
      if (row.getAskVolume() > m) m = row.getAskVolume();
    }
    maxAskVolume = m;
    return m;
  }

  public double getMaxVolume()
  {
    if (maxVolume != 0) return maxVolume;
    double m = 0;
    for(var row : rows.get()) {
      if (row.getVolume() > m) m = row.getVolume();
    }
    maxVolume = m;
    return m;
  }

  public double getMaxBidVolume()
  {
    if (maxBidVolume != 0) return maxBidVolume;
    double m = 0;
    for(var row : rows.get()) {
      if (row.getBidVolume() > m) m = row.getBidVolume();
    }
    maxBidVolume = m;
    return m;
  }

  public double getTotalDelta()
  {
    if (totalDelta != 0) return totalDelta;
    double m = 0;
    for(var row : rows.get()) m += row.getDelta();
    totalDelta = m;
    return m;
  }

  public double getTotalVolume() { return totalVolume; }
  public double getTotalBidVolume() { return totalBidVolume; }
  public double getTotalAskVolume() { return totalAskVolume; }

  public float getVALow(int va[])
  {
    if (va == null || va.length != 2 || rows == null || va[0] < 0 || va[0] >= rows.size()) return 0f;
    var row = rows.get(va[0]);
    return row.getStartPrice();
  }

  public float getVAHigh(int va[])
  {
    if (va == null || va.length != 2 || rows == null || va[1] < 0 || va[1] >= rows.size()) return 0f;
    var row = rows.get(va[1]);
    return rangeTicks <= 1 ? row.getStartPrice() : row.getEndPrice();
  }

  // Calculates the value area using the given percentage
  // Returns an integer array of two entries: top index, bottom index
  public int[] getValueArea(double per)
  {
    per = Util.round(per, 4);
    // Can we use the cached data?
    var inds = lastVA;
    var lPer = lastPer;
    if (inds != null && per == lPer) return inds;
    inds = lastVA = _getValueArea(per);
    lastPer = per;
    lastTotalVolume = totalVolume;
    return inds;
  }

  private int[] _getValueArea(double per)
  {
    var tmp = rows.get();
    var poc = getPOC();
    int pInd = rows.indexOf(poc);
    if (poc == null || Util.isEmpty(tmp)) return new int[] {-1, -1};

    double targetVol = totalVolume * per;
    double totalVol = poc.getVolume();
    if (totalVol >= targetVol) return new int[] {pInd, pInd}; // POC contains enough volume already

    // March 11, 2020 Updated using algorithm posted on TradingView for Morad
    // https://www.tradingview.com/support/solutions/43000502040-volume-profile/#:~:text=Point%20of%20Control%20(POC)%20%E2%80%93,with%20the%20highest%20traded%20volume.&text=Profile%20Low%20%E2%80%93%20The%20lowest%20reached,traded%20during%20the%20time%20period

    int ti = pInd-1, li = pInd+1;
    int size = tmp.size();
    while(totalVol < targetVol) {
      // Step 4. Look at two rows above POC and add the total volume of both
      double tv1 = 0;
      if (ti >= 0) tv1 += tmp.get(ti).getVolume();
      if (ti > 0) tv1 += tmp.get(ti-1).getVolume();

      // Step 5. Look at two rows below POC and add the total volume of both
      double tv2 = 0;
      if (li < size) tv2 += tmp.get(li).getVolume();
      if (li < size-1) tv2 += tmp.get(li+1).getVolume();

      if (tv1 > tv2) {
        totalVol += tv1;
        ti -= 2;
      }
      else {
        totalVol += tv2;
        li += 2;
      }
      if (ti < 0 && li >= size) break;
    }
    int top = ti+1;
    if (top < 0) top = 0;
    int bottom = li-1;
    if (bottom >= size) bottom = size-1;

    return new int[] { top, bottom };

    /*
    // These variable track the best range found so far.
    int bc=-1; // best count
    double bvol = 0; // best volume
    int bti=0, bbi=0; // best range indices

    // Optimization: since we need to include the POC, find the lower and upper bounds for the indices
    int lb=0, ub = tmp.size()-1;
    double vol = 0;
    for(int i=pocInd; i >= 0; i--) {
      vol += tmp.get(i).getVolume();
      if (vol < targetVol) continue;
      lb = i;
      break;
    }
    vol = 0;
    for(int i=pocInd; i < tmp.size(); i++) {
      vol += tmp.get(i).getVolume();
      if (vol < targetVol) continue;
      ub = i;
      break;
    }

    for(int ti=lb; ti <= ub; ti++) {
      vol = 0;
      for(int bi=ti; bi <= ub; bi++) { // find the bottom indices that fits our criteria
        vol += tmp.get(bi).getVolume();
        if (vol >= targetVol && pocInd >= ti && pocInd <= bi) { // Make sure to include the POC here
          int count = bi-ti;
          if (bc == -1 || count < bc || (count==bc && vol > bvol)) {
            bti = ti; bbi = bi;
            bc=count;
            bvol = vol;
          }
          break;
        }
      }
    }

    if (bc == -1) return new int[] {0, tmp.size()-1};
    return new int[] { bti, bbi }; */
  }

  public long getStartTime() { return startTime; }
  public long getEndTime() { return endTime; }
  public void setEndTime(long time) { endTime = time; }
  public long getInterval() { return interval; }
  public float getOpenPrice() { return openPrice; }
  public float getClosePrice() { return closePrice; }
  public float getHighPrice() { return highPrice; }
  public float getLowPrice() { return lowPrice; }
  public int getRangeTicks() { return rangeTicks; }
  public Instrument getInstrument() { return instrument; }

  public void setFilterTrades(boolean b) { filterTrades = b; }
  public boolean isFilterTrades() { return filterTrades; }
  public void setMinTradeSize(float s) { minTradeSize = s; }
  public float getMinTradeSize() { return minTradeSize; }

  public void onTick(Tick tick)
  {
    float p = tick.getPrice();
    var row = findOrCreate(p);
    row.onTick(tick);
    float vol = tick.getVolumeAsFloat();
    if (vol <= 0) vol = 1;
    totalVolume += vol;
    boolean ask = tick.isAskTick();
    if (ask) totalAskVolume += vol;
    else totalBidVolume += vol;

    if (openPrice == 0f) {
      openPrice = lowPrice = highPrice = p;
      minDelta = Double.MAX_VALUE;
      maxDelta = Double.MIN_NORMAL;
    }
    if (p < lowPrice) lowPrice = p;
    if (p > highPrice) highPrice = p;
    closePrice = p;
    totalDelta += ask ? vol : -vol;
    if (totalDelta < minDelta) minDelta = totalDelta;
    if (totalDelta > maxDelta) maxDelta = totalDelta;
    maxBidVolume = maxAskVolume = maxVolume = 0;

    // Jan 20, 2021  Calculating value area can be very expensive if there is a lot of rows.
    // To optimize look for cases where the number of rows is high and the volume change is small
    if (rows.size() < 1000 || (lastTotalVolume > 0 && totalVolume/lastTotalVolume > 1.001)) {
      lastVA = null; lastPer=-1;
    }
    ticks++;
    if (first == null) first = tick;
    last = tick;
    minRowDelta = Double.MAX_VALUE;
    maxRowDelta = Double.MIN_VALUE;
  }

  public List<VolumeRow> getRows() { return rows.get(); }

  public VolumeRow find(float price)
  {
    if (lastRow >= 0) {
      var r = rows.get(lastRow);
      if (r.contains(price)) return r;
      // Ticks are not random and tend to move a small amount from the last tick, this is the most efficient search pattern
      if (price < r.getStartPrice()) {
        for(int i=lastRow-1; i >= 0; i--) {
          r = rows.get(i);
          if (r.contains(price)) { lastRow=i; return r; }
        }
      }
      else {
        for(int i=lastRow+1; i < rows.size(); i++) {
          r = rows.get(i);
          if (r.contains(price)) { lastRow=i; return r; }
        }
      }
    }
    return null;
  }

  protected VolumeRow findOrCreate(float price)
  {
    price = round(price); // Need to make sure this is rounded, otherwise we can get overlapping rows
    var row = find(price);
    if (row != null) return row;

    float start = price, end = price + tickSize;
    if (rangeTicks > 1) {
      // Determine the price range.  This is a little tricky, since we need to know where to start.
      long p = Math.round(price/tickSize); // convert to a long and still keep the same precision
      long startPrice = (p/rangeTicks)*rangeTicks; // This should effectively compute the floor
      long endPrice = (p/rangeTicks)*rangeTicks + rangeTicks; // This should effectively compute the ceiling
      start = round(startPrice * tickSizeD);
      end = round(endPrice * tickSizeD);
    }
    else {
      end = round(end);
    }

    //System.err.println(start + " - " + end + " " + price);
    row = new VolumeRow(this, start, end);

    // Since we are using a ConcurrentList, we don't need to worry about concurrent updates.
    if (rows.isEmpty()) {
      rows.add(row);
      lastRow = 0;
    }
    else if (price >= rows.get(rows.size()-1).getEndPrice()) {
      rows.addNoCheck(row);
      lastRow = rows.size()-1;
    }
    else {
      for(int i=0; i < rows.size(); i++) { // insert this in order
        if (rows.get(i).getStartPrice() > price) {
          rows.addNoCheck(i, row);
          lastRow = i;
          return row;
        }
      }
    }
    return row;
  }
  private int lastRow=-1;

  private float round(double price) { return (float)Util.roundDouble(Util.round(price, tickSizeD), 9); }

  public VolumeRow getPOC()
  {
    if (pocRow == null) updatePOC();
    return pocRow;
  }

  public float getPOCMidpoint()
  {
    var poc = getPOC();
    if (poc == null) return 0f;
    if (rangeTicks <= 1) return poc.getStartPrice();
    return (poc.getEndPrice() + poc.getStartPrice())/2;
  }

  protected void updatePOC()
  {
    int i=0, mid = (rows.size()-1)/2;
    int pind = -1;
    VolumeRow poc = null;
    for(var row : rows.get()) {
      if (poc == null) { poc = row; pind = i; }
      else if (row.getVolume() > poc.getVolume()) { poc = row; pind = i; }
      else if (row.getVolume() == poc.getVolume()) {
        // Choose the one that is closer to the middle
        if (Math.abs(i-mid) < Math.abs(pind-mid)) {
          poc = row; pind = i;
        }
      }
      i++;
    }
    pocRow = poc;
    lastVA = null; // Make sure this gets recomputed
  }

  void setPOC(VolumeRow row)
  {
    pocRow = row;
    lastVA = null; // Make sure this gets recomputed
  }

  public int getTicks() { return ticks; }
  public Tick getFirst()  { return first; }
  public Tick getLast()  { return last; }

  public boolean isComplete() { return complete; }
  public void setComplete(boolean b)
  {
    complete = b;
    if (b) {
      // Make sure these get recomputed
      lastVA = null;
      lastPer = -1;
      pocRow = null;
    }
  }

  private Instrument instrument;
  private int rangeTicks;
  private long startTime, endTime;
  private long interval;
  private ConcurrentList<VolumeRow> rows = new ConcurrentList();
  private VolumeRow pocRow;
  protected double maxDelta=Double.MIN_VALUE, minDelta=Double.MAX_VALUE, totalDelta, maxBidVolume, maxAskVolume, maxVolume;
  protected double maxRowDelta=Double.MIN_VALUE, minRowDelta=Double.MAX_VALUE;
  protected double totalVolume, totalBidVolume, totalAskVolume;
  protected float openPrice, closePrice, lowPrice, highPrice;
  private float tickSize;
  private double tickSizeD;
  private VolumeProfile prev, next;
  private int ticks=0;
  private Tick first, last;
  private boolean complete = false; // Indicates that profile is complete and we can make sure that the VA and POC do not need to be recalculated

  private boolean filterTrades = false;
  private float minTradeSize = 0;

  // These variables are used to cache the ValueArea (since this can be an expensive operation)
  private int lastVA[];
  private double lastPer=-1;
  private double lastTotalVolume = 0;
}
