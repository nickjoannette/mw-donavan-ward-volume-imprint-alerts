package com.biiuse.motivewave;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.TextAttribute;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.motivewave.platform.sdk.common.BarSize;
import com.motivewave.platform.sdk.common.ColorInfo;
import com.motivewave.platform.sdk.common.DataContext;
import com.motivewave.platform.sdk.common.DataSeries;
import com.motivewave.platform.sdk.common.Defaults;
import com.motivewave.platform.sdk.common.DrawContext;
import com.motivewave.platform.sdk.common.FontInfo;
import com.motivewave.platform.sdk.common.Instrument;
import com.motivewave.platform.sdk.common.NVP;
import com.motivewave.platform.sdk.common.PathInfo;
import com.motivewave.platform.sdk.common.*;
import com.motivewave.platform.sdk.common.Tick;
import com.motivewave.platform.sdk.common.TickOperation;
import com.motivewave.platform.sdk.common.Util;
import com.motivewave.platform.sdk.common.X11Colors;
import com.motivewave.platform.sdk.common.desc.*;
import com.motivewave.platform.sdk.draw.Figure;
import com.motivewave.platform.sdk.study.Study;
import com.motivewave.platform.sdk.study.StudyHeader;

public class VolumeImprint extends Study
{

  class BSPP {
    public long time;
    public double price;
    public int imbLevel;
    public boolean signalled=false;
    public BSPP(long time, double price, int imbLevel){
      this.time=time;
      this.price=price;
      this.imbLevel=imbLevel;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj == null) {
        return false;
      }

      if (obj.getClass() != this.getClass()) {
        return false;
      }

      final BSPP other = (BSPP) obj;
      if (!(this.price == other.price && this.time == other.time)) {
        return false;
      }


      return true;
    }

  }

  protected ConcurrentList<BSPP> SupportBSPPs = new ConcurrentList<>();
  protected ConcurrentList<BSPP> AbsorptionBidBSPPs = new ConcurrentList<>();
  protected ConcurrentList<BSPP> AbsorptionAskBSPPs = new ConcurrentList<>();
  protected ConcurrentList<BSPP> ResistanceBSPPs = new ConcurrentList<>();

  // General
  protected final static String TYPE = "type", TYPE2 = "type2", COLUMN1_ENABLED = "c1Enabled", COLUMN2_ENABLED = "c2Enabled", TICK_INTERVAL = "tickInterval", IMPRINT_COUNT = "imprintCount",
          SHOW_ALL = "showAll", RTH_DATA = "rthData", SHOW_OC = "showOC", SHOW_BID_ASK_VOL = "showBidAskVol", USE_HISTORICAL_BARS = "useBars", ALIGN="align", CALC_ON_CLOSE="calcOnClose";
  // Display
  protected final static String BID_COLOR = "bidColor", ASK_COLOR = "askColor", AUTO_ADJUST_BA="autoAdjustBA", BID_ASK_LEVELS="bidAskLevels", BID_COLOR_LOW = "bcLow", BID_COLOR_HIGH = "bcHigh",
          ASK_COLOR_LOW = "acLow", ASK_COLOR_HIGH = "acHigh", BID_PER_LOW = "bpLow", BID_PER_HIGH = "bpHigh", ASK_PER_LOW="apLow", ASK_PER_HIGH = "apHigh",
          LINE_FONT = "lineFont", MAX_WIDTH = "maxWidth", MAX_WIDTH_ENABLED = "mwEnabled", NAKED_POC = "nakedPOC", BID_BOX = "bidBox", ASK_BOX = "askBox";
  // Imbalance
  protected final static String IMB_PER = "imbalancePer", IMB_DELTA1 = "imbDelta1", IMB_DELTA1_ENABLED = "imbDelta1Enabled",
          IMB_PER2 = "imbalancePer2", IMB_DELTA2 = "imbDelta2", IMB_DELTA2_ENABLED = "imbDelta2Enabled",
          IMB_PER3 = "imbalancePer3", IMB_DELTA3 = "imbDelta3", IMB_DELTA3_ENABLED = "imbDelta3Enabled",
          ASK_FONT = "askFont", BID_FONT = "bidBont", ASK_FONT2 = "askFont2", BID_FONT2 = "bidBont2", ASK_FONT3 = "askFont3", BID_FONT3 = "bidBont3",
          IMB_SLINE1 = "imbSLine1", IMB_RLINE1 = "imbRLine1", IMB_SLINE2 = "imbSLine2", IMB_RLINE2 = "imbRLine2",
          IMB_SLINE3 = "imbSLine3", IMB_RLINE3 = "imbRLine3", ABSORPTION_LEVEL = "absLevel";
  // Volume Profile
  protected final static String VP_ALIGN = "vpAlign", VP_LBL_FONT = "vpLblFont", VP_FONT_COLOR = "vpFontColor", SHOW_BID_ASK = "showBidAsk", VP_LBL_TYPE="vpLblType", VP_SCHEME="vpScheme", VP_SCHEME_ENABLED="vpSchemeEnabled",
          VP_BAR_COLOR = "vpBarColor", ETH_BAR_COLOR = "ethBarColor", SPLIT_ETH="splitETH", VP_BID_COLOR = "vpBidColor", VP_ASK_COLOR = "vpAskColor", VP_POC_COLOR = "vpPOCColor",
          VP_COLOR1 = "vpColor1", VP_COLOR2 = "vpColor2", SHOW_VP_RANGE = "showVPRange", VP_PERCENT_RANGE = "vpPerRange", VP_RANGE_BAR_COLOR = "vpRangeBarColor",
          VP_RANGE_FILL = "vpRangeFill", VP_RANGE_LINE = "vpRangeLine", VP_IMB="vpImb";
  // Ladder
  protected final static String ADJ_LADDER_COLOR="adjLadderColor", BID_HIST_COLOR = "bidHistColor", ASK_HIST_COLOR = "askHistColor", POC_LADDER_COLOR = "pocLColor", VALUE_LADDER_COLOR = "valueLColor", VALUE_RANGE_LADDER = "valueRangeL",
          L_LBL_FONT="lLblFont", L_FONT_COLOR="lFontColor", L_IMB="lImb", CANDLE_WIDTH="cdlWidth";
  // Delta Bars
  protected final static String D_SHOW_FILL="dShowFill", D_IMB="dImb", D_POC_BOX="dPocBox", D_HIST="dHist", D_HIST_ALIGN="dHistAlign", D_LBL_ALIGN="dLblAlign",
          D_LBL_FONT="dLblFont", D_FONT_COLOR="dFontColor", D_LBL_TYPE="dLblType", D_FILT="dFilt", D_FILT_ENABLED="dFiltEnabled";
  // Bid/Ask Bars
  protected final static String BA_SHOW_FILL="baShowFill", BA_IMB="baImb", BA_POC_BOX="baPocBox", BA_HIST="baHist", BA_HIST_ALIGN="baHistAlign", BA_LBL_ALIGN="baLblAlign",
          BA_LBL_FONT="baLblFont", BA_FONT_COLOR="baFontColor", BA_LBL_TYPE="baLblType", BID_ASK_SEP="baSep", SHOW_BA_ZEROS = "showBAZeros", BA_FILT="baFilt", BA_FILT_ENABLED="baFiltEnabled";
  // Volume Bars
  protected final static String V_SHOW_FILL="vShowFill", V_IMB="vImb", V_POC_BOX="vPocBox", V_HIST="vHist", V_HIST_ALIGN="vHistAlign", V_LBL_ALIGN="vLblAlign",
          V_LBL_FONT="vLblFont", V_FONT_COLOR="vFontColor", V_LBL_TYPE="vLblType", V_FILT="vFilt", V_FILT_ENABLED="vFiltEnabled";
  // Totals
  protected final static String SHOW_TOTALS = "showTotals", SHOW_VOLUME = "showVolume", SHOW_DELTA = "showDelta", SHOW_DELTA_DAY = "showDeltaDay",
          SHOW_DELTA_CHANGE = "showDeltaChange", SHOW_MIN_DELTA = "showMinDelta", SHOW_MAX_DELTA = "showMaxDelta", SHOW_DELTA_PER = "showDeltaPer", SHOW_AVAP = "showAVAP",
          SHOW_CUM_DELTA="showCumDelta", TOTAL_FONT = "totalFont", TOTAL_FONT_COLOR = "totalFontColor", POS_COLOR = "posColor", NEG_COLOR = "negColor", TOTAL_LEVELS="totalLevels", NEG_COLOR_LOW = "ncLow",
          NEG_COLOR_HIGH = "ncHigh", POS_COLOR_LOW = "pcLow", POS_COLOR_HIGH = "pcHigh", NEG_PER_LOW = "npLow", NEG_PER_HIGH = "npHigh", POS_PER_LOW="ppLow", POS_PER_HIGH = "ppHigh";
  // Candle Totals
  protected final static String POS_DELTA_FONT = "pdFont", NEG_DELTA_FONT = "ndFont", VOL_FONT = "volFont", BID_ASK_PER_FONT = "baPerFont";
  // Summary Profile/Ladder
  protected final static String SHOW_SUMMARY = "showSummary", SUMMARY_TYPE = "summaryType", SUMMARY_WIDTH = "summaryWidth", SUMMARY_OFFSET = "summaryOffset", SUMMARY_SPAN = "summarySpan",
          SUM_SPAN_CUSTOM="ssc", SUM_SPAN_UNIT="ssu", USE_CUST_SUM_SPAN="sscEnabled", SHOW_SUM_TOTALS="showSumTotals";
  // Summary Ranges
  protected final static String VISIBLE_BARS = "visibleBars", MIN = "min", DAY = "day", WEEK = "week", MONTH = "month";

  // Alignment
  protected final static String RIGHT = "RIGHT", LEFT = "LEFT", MIDDLE="MIDDLE", START="START", END="end";
  // Display Types
  protected final static String VOLUME = "volume", PROFILE = "profile", BID_ASK = "bidAsk", DELTA = "delta", LADDER = "ladder";
  // Profile Bar Color Options
  protected final static String VP_SCHEME_BID_ASK = "bidAsk", VP_SCHEME_DELTA="delta";
  // Trade Dots
  protected final static String DOT_ENABLED="dotEnabled", DOT_BID_FONT = "dotBidFont", DOT_ASK_FONT="dotAskFont", DOT_TRADE_SIZE="dotTradeSize", DOT_OUTLINE="dotOutline";
  final static String RECTANGLE="R", ROUNDED_RECTANGLE = "RR", CIRCLE = "C", NONE = "N", AUTO_CIRCLE="AC";

  protected final static Stroke STROKE2 = new BasicStroke(2f);
  protected final static Stroke STROKE1 = new BasicStroke(1f);

  protected final static String IMBALANCE_1_SUPPORT_SIGNAL="IMBALANCE_1_SUPPORT_SIGNAL";
  protected final static String IMBALANCE_2_SUPPORT_SIGNAL="IMBALANCE_2_SUPPORT_SIGNAL";
  protected final static String IMBALANCE_3_SUPPORT_SIGNAL="IMBALANCE_3_SUPPORT_SIGNAL";
  protected final static String IMBALANCE_1_RESISTANCE_SIGNAL="IMBALANCE_1_RESISTANCE_SIGNAL";
  protected final static String IMBALANCE_2_RESISTANCE_SIGNAL="IMBALANCE_2_RESISTANCE_SIGNAL";
  protected final static String IMBALANCE_3_RESISTANCE_SIGNAL="IMBALANCE_3_RESISTANCE_SIGNAL";
  protected final static String ABSORPTION_BID_SIGNAL="ABSORPTION_BID_SIGNAL";
  protected final static String ABSORPTION_ASK_SIGNAL="ABSORPTION_ASK_SIGNAL";
  protected final static int BID_LEVEL_INT = 4;
  protected final static int ASK_LEVEL_INT = 5;


  enum Signals {
    Imbalance1Resistance, Imbalance1Support,
    Imbalance2Resistance, Imbalance2Support,
    Imbalance3Resistance, Imbalance3Support,
    AbsorptionAsk, AbsorptionBid
  };


  @Override
  public VolumeImprint clone() {
    VolumeImprint study = null;
    profileMap.clear();
    summary = null;
    summaryImprint = null;
    imprints = new ArrayList<>();
    study = (VolumeImprint) super.clone();
    study.profileMap=new HashMap<>();
    study.summary = null;
    study.summaryImprint = null;
    study.calculator = null;
    study.calcInProgress = false;
    study.ethStarts = new ArrayList<>();
    study.imprints = Collections.EMPTY_LIST;
    study.imbSettings = new ArrayList<>();
    study.ResistanceBSPPs = new ConcurrentList<>();
    study.SupportBSPPs = new ConcurrentList<>();
    study.AbsorptionBidBSPPs = new ConcurrentList<>();
    study.AbsorptionAskBSPPs = new ConcurrentList<>();
    return study;
  }


  @Override
  public void onLoad(Defaults arg0)
  {
    profileMap.clear();
    summary = null;
    summaryImprint = null;
    imprints = new ArrayList<>();
    profileMap=new HashMap<>();
    summary = null;
    summaryImprint = null;
    calculator = null;
    calcInProgress = false;
    ethStarts = new ArrayList<>();
    imprints = Collections.EMPTY_LIST;
    imbSettings = new ArrayList<>();
    ResistanceBSPPs = new ConcurrentList<>();
    SupportBSPPs = new ConcurrentList<>();
    AbsorptionBidBSPPs = new ConcurrentList<>();
    AbsorptionAskBSPPs = new ConcurrentList<>();
    super.onLoad(arg0);
  }

  @Override
  public void initialize(Defaults defaults)
  {
    var df = defaults.getFont();
    var sd = createSD();

    List<NVP> types = new ArrayList();
    types.add(new NVP(get("LBL_PROFILE"), PROFILE));
    types.add(new NVP(get("LBL_BID_ASK"), BID_ASK));
    types.add(new NVP(get("LBL_LADDER"), LADDER));
    types.add(new NVP(get("LBL_DELTA"), DELTA));
    types.add(new NVP(get("LBL_VOLUME"), VOLUME));

    List<NVP> aligns = new ArrayList();
    aligns.add(new NVP(get("LBL_LEFT"), LEFT));
    aligns.add(new NVP(get("LBL_RIGHT"), RIGHT));

    List<NVP> aligns2 = new ArrayList();
    aligns2.add(new NVP(get("LBL_LEFT"), LEFT));
    aligns2.add(new NVP(get("LBL_MIDDLE"), MIDDLE));
    aligns2.add(new NVP(get("LBL_RIGHT"), RIGHT));

    List<NVP> barAligns = new ArrayList();
    barAligns.add(new NVP(get("LBL_MIDDLE"), MIDDLE));
    barAligns.add(new NVP(get("LBL_START_OF_BAR"), START));
    barAligns.add(new NVP(get("LBL_END_OF_BAR"), END));

    List<NVP> summaryTypes = new ArrayList();
    summaryTypes.add(new NVP(get("LBL_PROFILE"), PROFILE));
    summaryTypes.add(new NVP(get("LBL_LADDER"), LADDER));
    summaryTypes.add(new NVP(get("LBL_DELTA"), DELTA));

    List<NVP> spanTypes = new ArrayList();
    spanTypes.add(new NVP(get("LBL_VISIBLE_BARS"), VISIBLE_BARS));
    spanTypes.add(new NVP(get("LBL_DAY"), DAY));
    spanTypes.add(new NVP(get("LBL_WEEK"), WEEK));
    spanTypes.add(new NVP(get("LBL_MONTH"), MONTH));

    List<NVP> custUnits = new ArrayList();
    custUnits.add(new NVP(get("LBL_MIN"), MIN));
    custUnits.add(new NVP(get("LBL_DAY"), DAY));

    List<NVP> lblTypes = new ArrayList();
    lblTypes.add(new NVP(get("LBL_VOLUME"), VOLUME));
    lblTypes.add(new NVP(get("LBL_PROFILE"), PROFILE));
    lblTypes.add(new NVP(get("LBL_BID_ASK"), BID_ASK));
    lblTypes.add(new NVP(get("LBL_DELTA"), DELTA));

    List<NVP> volBarColors = new ArrayList();
    volBarColors.add(new NVP(get("LBL_BID_ASK"), VP_SCHEME_BID_ASK));
    volBarColors.add(new NVP(get("LBL_DELTA"), VP_SCHEME_DELTA));

    var bc = Util.getAlphaFill(defaults.getBarDownColor(), 255);
    var ac = Util.getAlphaFill(defaults.getBarUpColor(), 255).darker();

    var tab = sd.addTab(get("TAB_GENERAL"));
    var grp = tab.addGroup("", false);
    grp.addRow(new DiscreteDescriptor(TYPE, get("LBL_COLUMN1"), PROFILE, types), new BooleanDescriptor(COLUMN1_ENABLED, get("LBL_ENABLED"), true, false));
    grp.addRow(new DiscreteDescriptor(TYPE2, get("LBL_COLUMN2"), DELTA, types), new BooleanDescriptor(COLUMN2_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new IntegerDescriptor(TICK_INTERVAL, get("LBL_TICK_INTERVAL"), 1, 1, 999999, 1));
    grp.addRow(new IntegerDescriptor(IMPRINT_COUNT, get("LBL_IMPRINT_COUNT"), 20, 0, 9999, 1), new BooleanDescriptor(SHOW_ALL, get("LBL_SHOW_ALL"), false, false));
    grp.addRow(new DiscreteDescriptor(ALIGN, get("LBL_ALIGN"), MIDDLE, barAligns));
    grp.addRow(new BooleanDescriptor(RTH_DATA, get("LBL_RTH_DATA"), false, false));
    grp.addRow(new BooleanDescriptor(CALC_ON_CLOSE, get("LBL_CALC_ON_CLOSE"), false, false));
    grp.addRow(new BooleanDescriptor(USE_HISTORICAL_BARS, get("LBL_USE_HISTORICAL_BARS"), false, false));

    tab = sd.addTab(get("TAB_DISPLAY"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(SHOW_OC, get("LBL_SHOW_OC"), true, false));
    grp.addRow(new BooleanDescriptor(SHOW_BID_ASK_VOL, get("LBL_SHOW_BID_ASK_VOL"), false, false));
    grp.addRow(new ColorDescriptor(BID_COLOR, get("LBL_BID_COLOR"), bc), new SpacerDescriptor(8), new ColorDescriptor(ASK_COLOR, get("LBL_ASK_COLOR"), ac),
            new BooleanDescriptor(AUTO_ADJUST_BA, get("LBL_AUTO_ADJUST_BA"), true, false));
    grp.addRow(new BooleanDescriptor(BID_ASK_LEVELS, get("LBL_USE_LEVELS"), true, false));
    grp.addRow(new ColorDescriptor(BID_COLOR_LOW, get("LBL_BID_COLOR_LOW"), bc.brighter()), new IntegerDescriptor(BID_PER_LOW, "", 25, 1, 99, 1), new LabelDescriptor("%"), new SpacerDescriptor(8),
            new ColorDescriptor(BID_COLOR_HIGH, get("LBL_BID_COLOR_HIGH"), bc.darker()), new IntegerDescriptor(BID_PER_HIGH, "", 75, 1, 99, 1), new LabelDescriptor("%"));
    grp.addRow(new ColorDescriptor(ASK_COLOR_LOW, get("LBL_ASK_COLOR_LOW"), ac.brighter()), new IntegerDescriptor(ASK_PER_LOW, "", 25, 1, 99, 1), new LabelDescriptor("%"), new SpacerDescriptor(8),
            new ColorDescriptor(ASK_COLOR_HIGH, get("LBL_ASK_COLOR_HIGH"), ac.darker()), new IntegerDescriptor(ASK_PER_HIGH, "", 75, 1, 99, 1), new LabelDescriptor("%"));
    grp.addRow(new PathDescriptor(NAKED_POC, get("LBL_NAKED_POC"), defaults.getYellowLine(), 1f, null, false, false, true));
    grp.addRow(new FontDescriptor(LINE_FONT, get("LBL_LINE_FONT"), df, null, false, false, true));
    grp.addRow(new IntegerDescriptor(MAX_WIDTH, get("LBL_MAX_WIDTH"), 150, 50, 9999, 1), new BooleanDescriptor(MAX_WIDTH_ENABLED, get("LBL_ENABLED"), false, false));

    tab = sd.addTab(get("TAB_IMBALANCE"));
    grp = tab.addGroup("", false);
    var f1 = df.deriveFont(Font.BOLD);
    var f2 = f1.deriveFont(f1.getSize2D() + 1f);
    var f3 = f2.deriveFont(f2.getSize2D() + 1f);
    grp.addRow(new IntegerDescriptor(IMB_PER, get("LBL_IMBALANCE_PER"), 200, 1, 9999, 1), new IntegerDescriptor(IMB_DELTA1, get("LBL_IMB_DELTA_FILTER"), 100, 1, 9999, 1), new BooleanDescriptor(IMB_DELTA1_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new FontDescriptor(ASK_FONT, get("LBL_ASK_FONT"), f1, defaults.getGreenLine(), true, false, true));
    grp.addRow(new FontDescriptor(BID_FONT, get("LBL_BID_FONT"), f1, defaults.getRedLine(), true, false, true));
    grp.addRow(new PathDescriptor(IMB_RLINE1, get("LBL_IMB_RLINE1"), defaults.getRedLine(), 2f, null, false, false, true));
    grp.addRow(new PathDescriptor(IMB_SLINE1, get("LBL_IMB_SLINE1"), defaults.getGreenLine(), 2f, null, false, false, true));
    grp.addRow(new IntegerDescriptor(IMB_PER2, get("LBL_IMBALANCE_PER2"), 300, 1, 9999, 1), new IntegerDescriptor(IMB_DELTA2, get("LBL_IMB_DELTA_FILTER2"), 100, 1, 9999, 1), new BooleanDescriptor(IMB_DELTA2_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new FontDescriptor(ASK_FONT2, get("LBL_ASK_FONT2"), f2, defaults.getGreenLine(), true, false, true));
    grp.addRow(new FontDescriptor(BID_FONT2, get("LBL_BID_FONT2"), f2, defaults.getRedLine(), true, false, true));
    grp.addRow(new PathDescriptor(IMB_RLINE2, get("LBL_IMB_RLINE2"), defaults.getRedLine(), 2f, null, false, false, true));
    grp.addRow(new PathDescriptor(IMB_SLINE2, get("LBL_IMB_SLINE2"), defaults.getGreenLine(), 2f, null, false, false, true));
    grp.addRow(new IntegerDescriptor(IMB_PER3, get("LBL_IMBALANCE_PER3"), 400, 1, 9999, 1), new IntegerDescriptor(IMB_DELTA3, get("LBL_IMB_DELTA_FILTER3"), 100, 1, 9999, 1), new BooleanDescriptor(IMB_DELTA3_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new FontDescriptor(ASK_FONT3, get("LBL_ASK_FONT3"), f3, defaults.getGreenLine(), true, false, true));
    grp.addRow(new FontDescriptor(BID_FONT3, get("LBL_BID_FONT3"), f3, defaults.getRedLine(), true, false, true));
    grp.addRow(new PathDescriptor(IMB_RLINE3, get("LBL_IMB_RLINE3"), defaults.getRedLine(), 2f, null, false, false, true));
    grp.addRow(new PathDescriptor(IMB_SLINE3, get("LBL_IMB_SLINE3"), defaults.getGreenLine(), 2f, null, false, false, true));
    grp.addRow(new IntegerDescriptor(ABSORPTION_LEVEL, get("LBL_ABSORPTION_LEVEL"), 3, 1, 9999, 1));
    grp.addRow(new PathDescriptor(BID_BOX, get("LBL_BID_OUTLINE"), defaults.getRedLine(), 1f, null, false, false, true));
    grp.addRow(new PathDescriptor(ASK_BOX, get("LBL_ASK_OUTLINE"), defaults.getGreenLine(), 1f, null, false, false, true));

    tab = new SettingTab("Custom Signals");
    sd.addTab(tab);
    grp = new SettingGroup("Imbalance 1", false);
    tab.addGroup(grp);
    grp.addRow(new BooleanDescriptor(IMBALANCE_1_SUPPORT_SIGNAL, "Support Signal", true));
    grp.addRow(new BooleanDescriptor(IMBALANCE_1_RESISTANCE_SIGNAL, "Resistance Signal", true));
    grp = new SettingGroup("Imbalance 2", false);
    tab.addGroup(grp);
    grp.addRow(new BooleanDescriptor(IMBALANCE_2_SUPPORT_SIGNAL, "Support Signal", true));
    grp.addRow(new BooleanDescriptor(IMBALANCE_2_RESISTANCE_SIGNAL, "Resistance Signal", true));
    grp = new SettingGroup("Imbalance 3", false);
    tab.addGroup(grp);
    grp.addRow(new BooleanDescriptor(IMBALANCE_3_SUPPORT_SIGNAL, "Support Signal", true));
    grp.addRow(new BooleanDescriptor(IMBALANCE_3_RESISTANCE_SIGNAL, "Resistance Signal", true));

    grp = new SettingGroup("Absorption", false);
    tab.addGroup(grp);
    grp.addRow(new BooleanDescriptor(ABSORPTION_BID_SIGNAL, "Bid Absorption Signal", true));
    grp.addRow(new BooleanDescriptor(ABSORPTION_ASK_SIGNAL, "Ask Absorption Signal", true));

    tab = sd.addTab(get("LBL_VOLUME_PROFILE"));
    grp = tab.addGroup(get("LBL_DISPLAY"), false);
    grp.addRow(new DiscreteDescriptor(VP_ALIGN, get("LBL_VP_ALIGN"), LEFT, aligns));
    grp.addRow(new ColorDescriptor(VP_BAR_COLOR, get("LBL_BAR_COLOR"), Util.getAlphaFill(defaults.getBlue(), 160)),
            new ColorDescriptor(ETH_BAR_COLOR, "", Util.getAlphaFill(defaults.getGrey(), 160)),
            new BooleanDescriptor(SPLIT_ETH, get("LBL_SPLIT_ETH"), false, false));
    grp.addRow(new ColorDescriptor(VP_POC_COLOR, get("LBL_POC_BAR_COLOR"), Util.getAlphaFill(defaults.getOrange(), 160), true, true));
    grp.addRow(new DiscreteDescriptor(VP_SCHEME, get("LBL_VP_SCHEME"), VP_SCHEME_BID_ASK, volBarColors), new BooleanDescriptor(VP_SCHEME_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new ColorDescriptor(VP_COLOR1, get("LBL_VP_COLOR1"), defaults.getRed()));
    grp.addRow(new ColorDescriptor(VP_COLOR2, get("LBL_VP_COLOR2"), defaults.getGreen()));
    grp.addRow(new FontDescriptor(VP_LBL_FONT, get("LBL_LABEL_FONT"), df, defaults.getTextColor(), false, true, true));
    grp.addRow(new ColorDescriptor(VP_FONT_COLOR, get("LBL_LABEL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new DiscreteDescriptor(VP_LBL_TYPE, get("LBL_LABEL_TYPE"), PROFILE, lblTypes));
    grp.addRow(new BooleanDescriptor(VP_IMB, get("LBL_SHOW_IMBALANCE"), true, false));

    grp = tab.addGroup(get("LBL_VALUE_AREA"), false);
    grp.addRow(new BooleanDescriptor(SHOW_VP_RANGE, get("LBL_SHOW_VALUE_AREA"), false));
    grp.addRow(new DoubleDescriptor(VP_PERCENT_RANGE, get("LBL_PERCENT_RANGE"), 70, 1, 100, 0.1));
    grp.addRow(new ColorDescriptor(VP_RANGE_BAR_COLOR, get("LBL_BAR_COLOR"), Util.getAlphaFill(defaults.getGrey(), 160), true, true));
    grp.addRow(new PathDescriptor(VP_RANGE_LINE, get("LBL_RANGE_LINE"), defaults.getLineColor(), 1.0f, new float[] {3f, 3f}, true, false, true));
    grp.addRow(new ColorDescriptor(VP_RANGE_FILL, get("LBL_RANGE_FILL"), Util.getAlphaFill(defaults.getGrey()), true, true));

    tab = sd.addTab(get("TAB_LADDER"));
    grp = tab.addGroup("", false);
    grp.addRow(new FontDescriptor(L_LBL_FONT, get("LBL_LABEL_FONT"), df, defaults.getTextColor(), false, true, true));
    grp.addRow(new ColorDescriptor(L_FONT_COLOR, get("LBL_LABEL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new BooleanDescriptor(L_IMB, get("LBL_SHOW_IMBALANCE"), true, false));
    grp.addRow(new BooleanDescriptor(ADJ_LADDER_COLOR, get("LBL_ADJ_LADDER_COLOR"), true, false));
    grp.addRow(new IntegerDescriptor(CANDLE_WIDTH, get("LBL_CANDLE_WIDTH"), 14, 1, 99, 1));
    grp.addRow(new ColorDescriptor(BID_HIST_COLOR, get("LBL_BID_HIST_COLOR"), Util.getAlphaFill(defaults.getGrey(), 160), true, true));
    grp.addRow(new ColorDescriptor(ASK_HIST_COLOR, get("LBL_ASK_HIST_COLOR"), Util.getAlphaFill(defaults.getBlue(), 160), true, true));
    grp.addRow(new ColorDescriptor(POC_LADDER_COLOR, get("LBL_POC_LADDER_COLOR"), Util.getAlphaFill(defaults.getYellow(), 160), true, true));
    grp.addRow(new ColorDescriptor(VALUE_LADDER_COLOR, get("LBL_VALUE_LADDER_COLOR"), Util.getAlphaFill(defaults.getGrey(), 160), true, true));
    grp.addRow(new DoubleDescriptor(VALUE_RANGE_LADDER, get("LBL_PERCENT_RANGE"), 70, 1, 100, 0.1));

    tab = sd.addTab(get("TAB_BID_ASK"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(BA_SHOW_FILL, get("LBL_SHOW_FILL"), true, false));
    grp.addRow(new DiscreteDescriptor(BA_HIST_ALIGN, get("LBL_BAR_HIST"), LEFT, aligns2), new BooleanDescriptor(BA_HIST, get("LBL_ENABLED"), false, false));
    grp.addRow(new BooleanDescriptor(BA_IMB, get("LBL_SHOW_IMBALANCE"), true, false));
    grp.addRow(new PathDescriptor(BA_POC_BOX, get("LBL_POC_OUTLINE"), defaults.getYellowLine(), 1f, null, true, false, true));
    grp.addRow(new FontDescriptor(BA_LBL_FONT, get("LBL_LABEL_FONT"), df, defaults.getTextColor(), false, true, true));
    grp.addRow(new ColorDescriptor(BA_FONT_COLOR, get("LBL_LABEL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new DiscreteDescriptor(BA_LBL_TYPE, get("LBL_LABEL_TYPE"), BID_ASK, lblTypes));
    grp.addRow(new DiscreteDescriptor(BA_LBL_ALIGN, get("LBL_LABEL_ALIGN"), RIGHT, aligns2));
    grp.addRow(new IntegerDescriptor(BA_FILT, get("LBL_SIZE_FILTER"), 100, 1, 999999, 1), new BooleanDescriptor(BA_FILT_ENABLED, get("LBL_ENABLED"), false, false));
    grp.addRow(new StringDescriptor(BID_ASK_SEP, get("LBL_SEPARATOR"), " x ", 50), new SpacerDescriptor(8), new BooleanDescriptor(SHOW_BA_ZEROS, get("LBL_SHOW_BA_ZEROS"), true, false));

    tab = sd.addTab(get("TAB_DELTA"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(D_SHOW_FILL, get("LBL_SHOW_FILL"), true, false));
    grp.addRow(new DiscreteDescriptor(D_HIST_ALIGN, get("LBL_BAR_HIST"), LEFT, aligns2), new BooleanDescriptor(D_HIST, get("LBL_ENABLED"), false, false));
    grp.addRow(new BooleanDescriptor(D_IMB, get("LBL_SHOW_IMBALANCE"), true, false));
    grp.addRow(new PathDescriptor(D_POC_BOX, get("LBL_POC_OUTLINE"), defaults.getYellowLine(), 1f, null, true, false, true));
    grp.addRow(new FontDescriptor(D_LBL_FONT, get("LBL_LABEL_FONT"), df, defaults.getTextColor(), false, true, true));
    grp.addRow(new ColorDescriptor(D_FONT_COLOR, get("LBL_LABEL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new DiscreteDescriptor(D_LBL_TYPE, get("LBL_LABEL_TYPE"), DELTA, lblTypes));
    grp.addRow(new DiscreteDescriptor(D_LBL_ALIGN, get("LBL_LABEL_ALIGN"), RIGHT, aligns2));
    grp.addRow(new IntegerDescriptor(D_FILT, get("LBL_SIZE_FILTER"), 100, 1, 999999, 1), new BooleanDescriptor(D_FILT_ENABLED, get("LBL_ENABLED"), false, false));

    tab = sd.addTab(get("TAB_VOLUME"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(V_SHOW_FILL, get("LBL_SHOW_FILL"), true, false));
    grp.addRow(new DiscreteDescriptor(V_HIST_ALIGN, get("LBL_BAR_HIST"), LEFT, aligns2), new BooleanDescriptor(V_HIST, get("LBL_ENABLED"), false, false));
    grp.addRow(new BooleanDescriptor(V_IMB, get("LBL_SHOW_IMBALANCE"), true, false));
    grp.addRow(new PathDescriptor(V_POC_BOX, get("LBL_POC_OUTLINE"), defaults.getYellowLine(), 1f, null, true, false, true));
    grp.addRow(new FontDescriptor(V_LBL_FONT, get("LBL_LABEL_FONT"), df, defaults.getTextColor(), false, true, true));
    grp.addRow(new ColorDescriptor(V_FONT_COLOR, get("LBL_LABEL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new DiscreteDescriptor(V_LBL_TYPE, get("LBL_LABEL_TYPE"), VOLUME, lblTypes));
    grp.addRow(new DiscreteDescriptor(V_LBL_ALIGN, get("LBL_LABEL_ALIGN"), RIGHT, aligns2));
    grp.addRow(new IntegerDescriptor(V_FILT, get("LBL_SIZE_FILTER"), 100, 1, 999999, 1), new BooleanDescriptor(V_FILT_ENABLED, get("LBL_ENABLED"), false, false));

    List<NVP> outlines = new ArrayList();
    outlines.add(new NVP(get("LBL_RECTANGLE"), RECTANGLE));
    outlines.add(new NVP(get("LBL_ROUNDED_RECTANGLE"), ROUNDED_RECTANGLE));
    outlines.add(new NVP(get("LBL_CIRCLE"), CIRCLE));
    outlines.add(new NVP(get("LBL_AUTO_CIRCLE"), AUTO_CIRCLE));
    outlines.add(new NVP(get("LBL_NONE"), NONE));

    Font f = defaults.getFont();
    tab = sd.addTab(get("TAB_TRADE_DOTS"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(DOT_ENABLED, get("LBL_SHOW_TRADE_DOTS"), false));
    grp.addRow(new DiscreteDescriptor(DOT_OUTLINE, get("LBL_OUTLINE"), AUTO_CIRCLE, outlines));
    grp.addRow(new FontDescriptor(DOT_ASK_FONT, get("LBL_ASK_TRADE"), f, X11Colors.BLACK, true, defaults.getGreen(), true, true, false));
    grp.addRow(new FontDescriptor(DOT_BID_FONT, get("LBL_BID_TRADE"), f, X11Colors.WHITE, true, defaults.getRed(), true, true, false));
    grp.addRow(new IntegerDescriptor(DOT_TRADE_SIZE, get("LBL_MIN_TRADE_SIZE"), 100, 1, 9999999, 1));

    tab = sd.addTab(get("TAB_SUMMARY"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(SHOW_SUMMARY, get("LBL_SHOW_SUMMARY"), false));
    grp.addRow(new DiscreteDescriptor(SUMMARY_TYPE, get("LBL_TYPE"), PROFILE, summaryTypes));
    grp.addRow(new IntegerDescriptor(SUMMARY_WIDTH, get("LBL_SUMMARY_WIDTH"), 150, 1, 999, 1));
    grp.addRow(new IntegerDescriptor(SUMMARY_OFFSET, get("LBL_SUMMARY_OFFSET"), 0, 0, 999, 1));
    grp.addRow(new DiscreteDescriptor(SUMMARY_SPAN, get("LBL_SUMMARY_SPAN"), VISIBLE_BARS, spanTypes));
    grp.addRow(new IntegerDescriptor(SUM_SPAN_CUSTOM, get("LBL_SUMMARY_SPAN_CUSTOM"), 30, 1, 99, 1),
            new DiscreteDescriptor(SUM_SPAN_UNIT, "", MIN, custUnits),
            new BooleanDescriptor(USE_CUST_SUM_SPAN, get("LBL_ENABLED"), false, false));
    grp.addRow(new BooleanDescriptor(SHOW_SUM_TOTALS, get("LBL_SHOW_TOTALS"), true, false));

    tab = sd.addTab(get("TAB_CANDLE_TOTALS"));
    grp = tab.addGroup("", false);
    grp.addRow(new FontDescriptor(POS_DELTA_FONT, get("LBL_POS_DELTA_FONT"), df.deriveFont(df.getSize2D() + 1f), defaults.getGreenLine(), true, false, true));
    grp.addRow(new FontDescriptor(NEG_DELTA_FONT, get("LBL_NEG_DELTA_FONT"), df.deriveFont(df.getSize2D() + 1f), defaults.getRedLine(), true, false, true));
    grp.addRow(new FontDescriptor(VOL_FONT, get("LBL_VOL_FONT"), df.deriveFont(df.getSize2D() + 1f), defaults.getLineColor(), true, false, true));
    grp.addRow(new FontDescriptor(BID_ASK_PER_FONT, get("LBL_BID_ASK_PER_FONT"), df.deriveFont(df.getSize2D() - 1f), defaults.getLineColor(), true, false, true));

    tab = sd.addTab(get("TAB_TOTALS"));
    grp = tab.addGroup("", false);
    grp.addRow(new BooleanDescriptor(SHOW_TOTALS, get("LBL_SHOW_TOTALS"), false, false));
    grp.addRow(new BooleanDescriptor(SHOW_VOLUME, get("LBL_SHOW_VOLUME"), true, false), new SpacerDescriptor(8),
            new BooleanDescriptor(SHOW_AVAP, get("LBL_SHOW_AVAP"), false, false));
    grp.addRow(new BooleanDescriptor(SHOW_DELTA, get("LBL_SHOW_DELTA"), true, false), new SpacerDescriptor(8),
            new BooleanDescriptor(SHOW_CUM_DELTA, get("LBL_SHOW_CUM_DELTA"), false, false), new SpacerDescriptor(8),
            new BooleanDescriptor(SHOW_DELTA_CHANGE, get("LBL_SHOW_DELTA_CHANGE"), true, false));
    grp.addRow(new BooleanDescriptor(SHOW_MIN_DELTA, get("LBL_SHOW_MIN_DELTA"), false, false), new SpacerDescriptor(8),
            new BooleanDescriptor(SHOW_MAX_DELTA, get("LBL_SHOW_MAX_DELTA"), false, false), new SpacerDescriptor(8),
            new BooleanDescriptor(SHOW_DELTA_PER, get("LBL_SHOW_DELTA_PER"), false, false));
    grp.addRow(new FontDescriptor(TOTAL_FONT, get("LBL_FONT"), defaults.getFont()));
    grp.addRow(new ColorDescriptor(TOTAL_FONT_COLOR, get("LBL_FONT_COLOR"), defaults.getTextColor(), false, true));
    grp.addRow(new ColorDescriptor(POS_COLOR, get("LBL_POSITIVE_COLOR"), ac, true, true));
    grp.addRow(new ColorDescriptor(NEG_COLOR, get("LBL_NEGATIVE_COLOR"), bc, true, true));
    grp.addRow(new BooleanDescriptor(TOTAL_LEVELS, get("LBL_USE_LEVELS"), true));
    grp.addRow(new ColorDescriptor(POS_COLOR_LOW, get("LBL_POS_COLOR_LOW"), ac.brighter()), new IntegerDescriptor(POS_PER_LOW, "", 25, 1, 99, 1), new LabelDescriptor("%"));
    grp.addRow(new ColorDescriptor(POS_COLOR_HIGH, get("LBL_POS_COLOR_HIGH"), ac.darker()), new IntegerDescriptor(POS_PER_HIGH, "", 75, 1, 99, 1), new LabelDescriptor("%"));
    grp.addRow(new ColorDescriptor(NEG_COLOR_LOW, get("LBL_NEG_COLOR_LOW"), bc.brighter()), new IntegerDescriptor(NEG_PER_LOW, "", 25, 1, 99, 1), new LabelDescriptor("%"));
    grp.addRow(new ColorDescriptor(NEG_COLOR_HIGH, get("LBL_NEG_COLOR_HIGH"), bc.darker()), new IntegerDescriptor(NEG_PER_HIGH, "", 75, 1, 99, 1), new LabelDescriptor("%"));

    // Dependencies
    sd.addDependency(new EnabledDependency(false, SHOW_ALL, IMPRINT_COUNT));
    var d = new EnabledDependency(COLUMN2_ENABLED, TYPE2);
    d.setSource2(COLUMN1_ENABLED);
    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(COLUMN1_ENABLED, TYPE, COLUMN2_ENABLED));
    d = new EnabledDependency(BA_HIST, BA_HIST_ALIGN);
    d.setSource2(BA_SHOW_FILL);
    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(BA_SHOW_FILL, BA_HIST));
    d = new EnabledDependency(D_HIST, D_HIST_ALIGN);
    d.setSource2(D_SHOW_FILL);
    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(D_SHOW_FILL, D_HIST));
    d = new EnabledDependency(V_HIST, V_HIST_ALIGN);
    d.setSource2(V_SHOW_FILL);
    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(V_SHOW_FILL, V_HIST));
    sd.addDependency(new EnabledDependency(L_LBL_FONT, L_FONT_COLOR));
    sd.addDependency(new EnabledDependency(BA_LBL_FONT, BA_FONT_COLOR, BA_LBL_TYPE, BA_LBL_ALIGN));
    sd.addDependency(new EnabledDependency(D_LBL_FONT, D_FONT_COLOR, D_LBL_TYPE, D_LBL_ALIGN));
    sd.addDependency(new EnabledDependency(V_LBL_FONT, V_FONT_COLOR, V_LBL_TYPE, V_LBL_ALIGN));
    sd.addDependency(new EnabledDependency(MAX_WIDTH_ENABLED, MAX_WIDTH));
    sd.addDependency(new EnabledDependency(SHOW_VP_RANGE, VP_RANGE_BAR_COLOR, VP_PERCENT_RANGE, VP_RANGE_LINE, VP_RANGE_FILL));
    sd.addDependency(new EnabledDependency(SHOW_BID_ASK, VP_BID_COLOR, VP_ASK_COLOR));
    d = new EnabledDependency(BID_ASK_LEVELS, BID_COLOR_LOW, BID_COLOR_HIGH, BID_PER_LOW, BID_PER_HIGH, ASK_COLOR_LOW, ASK_COLOR_HIGH, ASK_PER_LOW, ASK_PER_HIGH);
    sd.addDependency(d);
    d = new EnabledDependency(BID_BOX, ABSORPTION_LEVEL);
    d.setOrCompare(true);
    d.setSource2(ASK_BOX);
    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(SHOW_TOTALS, TOTAL_LEVELS, SHOW_VOLUME, SHOW_DELTA, SHOW_CUM_DELTA, SHOW_DELTA_DAY, SHOW_DELTA_CHANGE, SHOW_MIN_DELTA, SHOW_MAX_DELTA,
            SHOW_DELTA_PER, SHOW_AVAP, TOTAL_FONT, POS_COLOR, NEG_COLOR, TOTAL_FONT_COLOR));
    d = new EnabledDependency(TOTAL_LEVELS, POS_COLOR_LOW, POS_COLOR_HIGH, POS_PER_LOW, POS_PER_HIGH);
    d.setSource2(SHOW_TOTALS);
    d.setSource3(POS_COLOR);
    sd.addDependency(d);
    d = new EnabledDependency(TOTAL_LEVELS, NEG_COLOR_LOW, NEG_COLOR_HIGH, NEG_PER_LOW, NEG_PER_HIGH);
    d.setSource2(SHOW_TOTALS);
    d.setSource3(NEG_COLOR);

    sd.addDependency(d);
    sd.addDependency(new EnabledDependency(VP_SCHEME_ENABLED, VP_SCHEME, VP_COLOR1, VP_COLOR2));
    sd.addDependency(new EnabledDependency(SPLIT_ETH, ETH_BAR_COLOR));
    sd.addDependency(new EnabledDependency(VP_LBL_FONT, VP_FONT_COLOR, VP_LBL_TYPE));
    sd.addDependency(new EnabledDependency(VALUE_LADDER_COLOR, VALUE_RANGE_LADDER));
    sd.addDependency(new EnabledDependency(DOT_ENABLED, DOT_ASK_FONT, DOT_BID_FONT, DOT_OUTLINE, DOT_TRADE_SIZE));
    //SUM_SPAN_CUSTOM="ssc", SUM_SPAN_UNIT="ssu", USE_CUST_SUM_SPAN="sscEnabled",
    sd.addDependency(new EnabledDependency(SHOW_SUMMARY, SUMMARY_TYPE, SUMMARY_WIDTH, SUMMARY_OFFSET, SUMMARY_SPAN, SUM_SPAN_CUSTOM, SUM_SPAN_UNIT, USE_CUST_SUM_SPAN, SHOW_SUM_TOTALS));
    sd.addDependency(new EnabledDependency(BA_FILT_ENABLED, BA_FILT));
    sd.addDependency(new EnabledDependency(D_FILT_ENABLED, D_FILT));
    sd.addDependency(new EnabledDependency(V_FILT_ENABLED, V_FILT));
    sd.addDependency(new EnabledDependency(IMB_DELTA1_ENABLED, IMB_DELTA1));
    sd.addDependency(new EnabledDependency(IMB_DELTA2_ENABLED, IMB_DELTA2));
    sd.addDependency(new EnabledDependency(IMB_DELTA3_ENABLED, IMB_DELTA3));
    d = new EnabledDependency(USE_CUST_SUM_SPAN, SUM_SPAN_CUSTOM, SUM_SPAN_UNIT);
    d.setSource2(SHOW_SUMMARY);
    sd.addDependency(d);
    d = new EnabledDependency(false, USE_CUST_SUM_SPAN, SUMMARY_SPAN);
    d.setSource2(SHOW_SUMMARY);
    sd.addDependency(d);

    sd.addQuickSettings(TYPE, COLUMN1_ENABLED, TYPE2, COLUMN2_ENABLED, TICK_INTERVAL, IMPRINT_COUNT, ALIGN, RTH_DATA, CALC_ON_CLOSE, USE_HISTORICAL_BARS,
            SHOW_OC, SHOW_BID_ASK_VOL, BID_COLOR, ASK_COLOR);
    sd.rowAlign(TYPE, COLUMN1_ENABLED);
    sd.rowAlign(TYPE2, COLUMN2_ENABLED);

    var desc = createRD();
    desc.setIDSettings(TYPE, TICK_INTERVAL, RTH_DATA);
    desc.setLabelSettings(TYPE, TICK_INTERVAL);
    desc.setLabelPrefix("Imbalance With Alerts v2.0");
    desc.declareSignal(Signals.Imbalance1Resistance, "Imbalance 1 Resistance");
    desc.declareSignal(Signals.Imbalance1Support, "Imbalance 1 Support");
    desc.declareSignal(Signals.Imbalance2Resistance, "Imbalance 2 Resistance");
    desc.declareSignal(Signals.Imbalance2Support, "Imbalance 2 Support");
    desc.declareSignal(Signals.Imbalance3Resistance, "Imbalance 3 Resistance");
    desc.declareSignal(Signals.Imbalance3Support, "Imbalance 3 Support");
    desc.declareSignal(Signals.AbsorptionBid, "Bid Absorption");
    desc.declareSignal(Signals.AbsorptionAsk, "Ask Absorption");
    desc.exportValue(new ValueDescriptor(Signals.Imbalance1Resistance, "Imb 1 Resistance", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.Imbalance1Support, "Imb 1 Support", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.Imbalance2Resistance, "Imb 2 Resistance", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.Imbalance2Support, "Imb 2 Support", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.Imbalance3Resistance, "Imb 3 Resistance", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.Imbalance3Support, "Imb 3 Support", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.AbsorptionBid, "Bid Absorption", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));
    desc.exportValue(new ValueDescriptor(Signals.AbsorptionAsk, "Ask Absorption", new String[] {TYPE, TICK_INTERVAL, IMPRINT_COUNT, IMB_PER, IMB_PER2, IMB_PER3}));

  }



  @Override
  public int getMinBars()
  {
    if (getSettings().getBoolean(SHOW_ALL, false)) return super.getMinBars();
    return getSettings().getInteger(IMPRINT_COUNT, 10);
  }

  @Override
  public Long getMinStartTime(DataContext ctx)
  {
    var s = getSettings();
    spanType = s.getString(SUMMARY_SPAN, VISIBLE_BARS);
    showSummary = s.getBoolean(SHOW_SUMMARY, false);
    custSumSpan = s.getInteger(SUM_SPAN_CUSTOM, 30);
    useCustSumSpan = s.getBoolean(USE_CUST_SUM_SPAN, false);
    custSumSpanUnits = s.getString(SUM_SPAN_UNIT, MIN);

    if (!showSummary || isSpanVisibleBars()) return super.getMinStartTime();
    return getSummaryStart(ctx);
  }

  private boolean isSpanVisibleBars() { return !useCustSumSpan && Util.compare(spanType, VISIBLE_BARS); }
  private boolean isCustomMinSummary() { return showSummary && useCustSumSpan && Util.compare(custSumSpanUnits, MIN); }

  private long getSummaryStart(DataContext ctx)
  {
    var ds = ctx.getDataSeries();
    if (ds == null) return -1;
    var _instr = ctx.getInstrument();
    if (_instr == null) return -1;
    long last = _instr.getLastTimestamp();
    if (last <= 0) last = ctx.getCurrentTime();

    if (!useCustSumSpan) {
      if (Util.compare(spanType, VISIBLE_BARS)) return ds.getVisibleStartTime();
      switch(spanType) {
        case WEEK: return _instr.getStartOfWeek(last, rthData);
        case MONTH: return _instr.getStartOfMonth(last, rthData);
      }
      return _instr.getStartOfDay(last, rthData);
    }

    if (Util.compare(custSumSpanUnits, MIN)) {
      long span = custSumSpan*Util.MILLIS_IN_MINUTE;
      long begin = last - span;
      // Round to a minute
      begin = Util.roundMinutes(begin, 1);
      if (last - begin > span) begin += Util.MILLIS_IN_MINUTE;
      return begin;
    }
    // Days
    long begin = _instr.getStartOfDay(last, rthData);
    for(int i = 0; i < custSumSpan; i++) {
      begin = Util.getStartOfPrevDay(begin, _instr, rthData);
    }
    return begin;
  }

  private long getSummaryEnd(DataContext ctx)
  {
    var ds = ctx.getDataSeries();
    var i = ctx.getInstrument();
    if (ds == null) return -1;
    if (i == null) return ds.getVisibleEndTime();
    long last = i.getLastTimestamp();
    if (last <= 0) last = ctx.getCurrentTime();
    if (!useCustSumSpan) {
      switch(spanType) {
        case VISIBLE_BARS: return ds.getVisibleEndTime();
        case WEEK: return i.getEndOfWeek(ctx.getCurrentTime(), rthData);
        case MONTH: return i.getEndOfMonth(ctx.getCurrentTime(), rthData);
      }
      return i.getEndOfDay(last, rthData);
    }
    if (Util.compare(custSumSpanUnits, MIN)) {
      long span = custSumSpan*Util.MILLIS_IN_MINUTE;
      return getSummaryStart(ctx) + span;
    }
    return i.getEndOfDay(last, rthData);
  }

  @Override
  public void clearState()
  {
    super.clearState();
    clearFigures();
    profileMap.clear();
    summary = null;
    summaryImprint = null;
    imprints = new ArrayList<>();
    var settings = getSettings();

    // This is generally not considered to be a good practice.
    // In this case the overhead of pulling values from the settings does register with the profiler (even though its a simple HashMap lookup).
    // Since there are many TPO figures and each figure is composed of bars and volume profile bars, the lookup on the settings object
    // can potentially be thousands for each draw operation.
    absLevel = settings.getInteger(ABSORPTION_LEVEL, 3);
    bidBox = settings.getPath(BID_BOX);
    askBox = settings.getPath(ASK_BOX);
    bidHist = settings.getColorInfo(BID_HIST_COLOR);
    askHist = settings.getColorInfo(ASK_HIST_COLOR);
    lineFont = settings.getFont(LINE_FONT);
    totalFont = settings.getFont(TOTAL_FONT);
    totalFontColor = settings.getColorInfo(TOTAL_FONT_COLOR);
    pFont = settings.getFont(VP_LBL_FONT);
    tickInterval = settings.getInteger(TICK_INTERVAL, 1);
    barAlign = settings.getString(ALIGN, MIDDLE).intern();
    rthData = settings.getBoolean(RTH_DATA, false);
    calcOnClose = settings.getBoolean(CALC_ON_CLOSE, false);
    ethBarColor = settings.getColor(ETH_BAR_COLOR);

    useHistoricalBars = settings.getBoolean(USE_HISTORICAL_BARS, false);
    type = settings.getString(TYPE, PROFILE).intern();
    type2 = settings.getString(TYPE2, DELTA).intern();
    col1Enabled = settings.getBoolean(COLUMN1_ENABLED, true);
    col2Enabled = settings.getBoolean(COLUMN2_ENABLED, false);
    summaryType = settings.getString(SUMMARY_TYPE, PROFILE).intern();
    nakedPoc = settings.getPath(NAKED_POC);
    askColor = settings.getColor(ASK_COLOR);
    bidColor = settings.getColor(BID_COLOR);
    autoAdjustBa = settings.getBoolean(AUTO_ADJUST_BA, true);
    baLevelsEnabled = settings.getBoolean(BID_ASK_LEVELS, false);
    askColorLow = settings.getColor(ASK_COLOR_LOW);
    askColorHigh = settings.getColor(ASK_COLOR_HIGH);
    askPerLow = settings.getInteger(ASK_PER_LOW, 25)/100d;
    askPerHigh = settings.getInteger(ASK_PER_HIGH, 75)/100d;
    bidColorLow = settings.getColor(BID_COLOR_LOW);
    bidColorHigh = settings.getColor(BID_COLOR_HIGH);
    bidPerLow = settings.getInteger(BID_PER_LOW, 25)/100d;
    bidPerHigh = settings.getInteger(BID_PER_HIGH, 75)/100d;
    spanType = settings.getString(SUMMARY_SPAN, VISIBLE_BARS).intern();
    custSumSpan = settings.getInteger(SUM_SPAN_CUSTOM, 30);
    useCustSumSpan = settings.getBoolean(USE_CUST_SUM_SPAN, false);
    custSumSpanUnits = settings.getString(SUM_SPAN_UNIT, MIN);
    showSummary = settings.getBoolean(SHOW_SUMMARY, false);
    showSumTotals = settings.getBoolean(SHOW_SUM_TOTALS, true);

    valueLadderColor = settings.getColorInfo(VALUE_LADDER_COLOR);
    pocLadderColor = settings.getColorInfo(POC_LADDER_COLOR);
    valueRangeLadder = settings.getInteger(VALUE_RANGE_LADDER, 70);
    lLblFont = settings.getFont(L_LBL_FONT);
    lFontColor = settings.getColorInfo(L_FONT_COLOR);
    lShowImb = settings.getBoolean(L_IMB, true);
    adjLadderColor = settings.getBoolean(ADJ_LADDER_COLOR, true);
    candleWidth = settings.getInteger(CANDLE_WIDTH, 14);

    baShowFill = settings.getBoolean(BA_SHOW_FILL, true);
    baHist = settings.getBoolean(BA_HIST, false);
    baHistAlign = settings.getString(BA_HIST_ALIGN, LEFT).intern();
    baLblAlign = settings.getString(BA_LBL_ALIGN, RIGHT).intern();
    baLblType = settings.getString(BA_LBL_TYPE, BID_ASK).intern();
    baFont = settings.getFont(BA_LBL_FONT);
    baFontColor = settings.getColorInfo(BA_FONT_COLOR);
    baPocBox = settings.getPath(BA_POC_BOX);
    baShowImb = settings.getBoolean(BA_IMB, true);
    baSep = settings.getString(BID_ASK_SEP, " x ").intern();
    showZeros = settings.getBoolean(SHOW_BA_ZEROS, true);
    baFiltEnabled = settings.getBoolean(BA_FILT_ENABLED, false);
    baFilt = settings.getInteger(BA_FILT, 100);

    dShowFill = settings.getBoolean(D_SHOW_FILL, true);
    dHist = settings.getBoolean(D_HIST, false);
    dHistAlign = settings.getString(D_HIST_ALIGN, LEFT).intern();
    dLblAlign = settings.getString(D_LBL_ALIGN, RIGHT).intern();
    dLblType = settings.getString(D_LBL_TYPE, DELTA).intern();
    dFont = settings.getFont(D_LBL_FONT);
    dFontColor = settings.getColorInfo(D_FONT_COLOR);
    dPocBox = settings.getPath(D_POC_BOX);
    dShowImb = settings.getBoolean(D_IMB, true);
    dFiltEnabled = settings.getBoolean(D_FILT_ENABLED, false);
    dFilt = settings.getInteger(D_FILT, 100);

    vShowFill = settings.getBoolean(V_SHOW_FILL, true);
    vHist = settings.getBoolean(V_HIST, false);
    vHistAlign = settings.getString(V_HIST_ALIGN, LEFT).intern();
    vLblAlign = settings.getString(V_LBL_ALIGN, RIGHT).intern();
    vLblType = settings.getString(V_LBL_TYPE, VOLUME).intern();
    vFont = settings.getFont(V_LBL_FONT);
    vFontColor = settings.getColorInfo(V_FONT_COLOR);
    vPocBox = settings.getPath(V_POC_BOX);
    vShowImb = settings.getBoolean(V_IMB, true);
    vFiltEnabled = settings.getBoolean(V_FILT_ENABLED, false);
    vFilt = settings.getInteger(V_FILT, 100);

    vpAlign = settings.getString(VP_ALIGN, LEFT).intern();
    vpShowImb = settings.getBoolean(VP_IMB, true);
    showVpRange = settings.getBoolean(SHOW_VP_RANGE);
    vpRangeFill = settings.getColorInfo(VP_RANGE_FILL);
    vpRangeLine = settings.getPath(VP_RANGE_LINE);
    vpColor1 = settings.getColor(VP_COLOR1, settings.getColor(VP_BID_COLOR));
    vpColor2 = settings.getColor(VP_COLOR2, settings.getColor(VP_ASK_COLOR));
    vpBarColor = settings.getColor(VP_BAR_COLOR);
    pLblType = settings.getString(VP_LBL_TYPE, VOLUME).intern();
    vpSchemeEnabled = settings.getBoolean(VP_SCHEME_ENABLED, !settings.getBoolean(SHOW_BID_ASK, false));
    vpScheme = settings.getString(VP_SCHEME, VP_SCHEME_BID_ASK).intern();
    volFont = settings.getFont(VOL_FONT);
    pFontColor = settings.getColorInfo(VP_FONT_COLOR);
    bidAskPerFont = settings.getFont(BID_ASK_PER_FONT);
    posDeltaFont = settings.getFont(POS_DELTA_FONT);
    negDeltaFont = settings.getFont(NEG_DELTA_FONT);
    posColor = settings.getColorInfo(POS_COLOR);
    negColor = settings.getColorInfo(NEG_COLOR);
    totalLevels = settings.getBoolean(TOTAL_LEVELS, false);
    posColorLow = settings.getColor(POS_COLOR_LOW);
    posColorHigh = settings.getColor(POS_COLOR_HIGH);
    posPerLow = settings.getInteger(POS_PER_LOW, 25)/100d;
    posPerHigh = settings.getInteger(POS_PER_HIGH, 75)/100d;
    negColorLow = settings.getColor(NEG_COLOR_LOW);
    negColorHigh = settings.getColor(NEG_COLOR_HIGH);
    negPerLow = settings.getInteger(NEG_PER_LOW, 25)/100d;
    negPerHigh = settings.getInteger(NEG_PER_HIGH, 75)/100d;

    dotsEnabled = settings.getBoolean(DOT_ENABLED, false);
    dotOutline = settings.getString(DOT_OUTLINE, AUTO_CIRCLE).intern();
    dotAskFont = settings.getFont(DOT_ASK_FONT);
    dotBidFont = settings.getFont(DOT_BID_FONT);
    dotMinSize = settings.getInteger(DOT_TRADE_SIZE, 100);

    summaryWidth = settings.getInteger(SUMMARY_WIDTH, 150);
    summaryOffset = settings.getInteger(SUMMARY_OFFSET, 0);
    showOC = settings.getBoolean(SHOW_OC, true);
    showBidAskVol= settings.getBoolean(SHOW_BID_ASK_VOL, false);
    vpPocColor = settings.getColorInfo(VP_POC_COLOR);
    maxWidthEnabled = settings.getBoolean(MAX_WIDTH_ENABLED, false);
    maxWidth = settings.getInteger(MAX_WIDTH, 150);
    vpPercentRange = settings.getInteger(VP_PERCENT_RANGE);
    vpRangeBarColor = settings.getColorInfo(VP_RANGE_BAR_COLOR);

    showTotals = settings.getBoolean(SHOW_TOTALS, false);;
    showAVAP = settings.getBoolean(SHOW_AVAP, false);
    showDeltaPer = settings.getBoolean(SHOW_DELTA_PER, false);
    showMaxDelta = settings.getBoolean(SHOW_MAX_DELTA, false);
    showMinDelta = settings.getBoolean(SHOW_MIN_DELTA, false);
    showDeltaChange = settings.getBoolean(SHOW_DELTA_CHANGE, true);
    showDeltaDay = settings.getBoolean(SHOW_DELTA_DAY, false);
    showDelta = settings.getBoolean(SHOW_DELTA, true);
    showCumDelta = settings.getBoolean(SHOW_CUM_DELTA, false);
    showVolume = settings.getBoolean(SHOW_VOLUME, true);


    // For convenience, combine separate imbalance settings into a single list
    var tmp = new ArrayList<ImbalanceSetting>();
    tmp.add(new ImbalanceSetting(0, settings.getFont(BID_FONT), settings.getFont(ASK_FONT), settings.getPath(IMB_SLINE1), settings.getPath(IMB_RLINE1), settings.getInteger(IMB_PER, 200)/100.0, settings.getInteger(IMB_DELTA1, 100), settings.getBoolean(IMB_DELTA1_ENABLED, false)));
    tmp.add(new ImbalanceSetting(1, settings.getFont(BID_FONT2), settings.getFont(ASK_FONT2), settings.getPath(IMB_SLINE2), settings.getPath(IMB_RLINE2), settings.getInteger(IMB_PER2, 300)/100.0, settings.getInteger(IMB_DELTA2, 100), settings.getBoolean(IMB_DELTA2_ENABLED, false)));
    tmp.add(new ImbalanceSetting(2, settings.getFont(BID_FONT3), settings.getFont(ASK_FONT3), settings.getPath(IMB_SLINE3), settings.getPath(IMB_RLINE3), settings.getInteger(IMB_PER3, 400)/100.0, settings.getInteger(IMB_DELTA3, 100), settings.getBoolean(IMB_DELTA3_ENABLED, false)));
    tmp.sort((s1, s2) -> Double.compare(s2.per, s1.per)); // sort descending by percentage
    imbSettings = tmp;
  }

  @Override
  public void onTick(DataContext ctx, Tick tick)
  {
    if (calcInProgress || instr == null || !instr.isInsideTradingHours(tick.getTime(), rthData) || calculator == null) return;
    var series = ctx.getDataSeries();
    if (series == null || series.size() <= 2) return;
    var prev = calculator.vp; // Hack: use this to check if we had a new volume profile added
    if (series.getValue(series.size()-1, "VP") == null) { // This will work for both Linear and Non-Linear Bars
      // Rollover to the new bar
      if (calcOnClose) {
        calculate(ctx);
        series.setValue(series.size()-1, "VP", "VP");
      }
      else {
        calculator.end = series.getEndTime(series.size()-2); // This should trigger the creation of a new VolumeProfile
        calculator.onTick(tick);
        if (summary != null && summary.getEndTime() > ctx.getCurrentTime()) summary.onTick(tick);
      }
    }
    else {
      calculator.onTick(tick);
      
      if (summary != null && summary.getEndTime() > ctx.getCurrentTime()) summary.onTick(tick);
    }
    if (prev != calculator.vp) Util.schedule(() -> updateFigures(ctx));
    else if (isCustomMinSummary() && summary != null && tick.getTime() > summary.getEndTime()) {
      summary = null;
      // For custom minute summary, its a rolling summary every minute;
      Util.schedule(() -> {
        var sum = new SummaryProfile(getSummaryStart(ctx), getSummaryEnd(ctx), instr, tickInterval);
        instr.forEachTick(sum.getStartTime(), ctx.getCurrentTime() + 5*Util.MILLIS_IN_MINUTE, ctx.isRTH(), useHistoricalBars, t -> sum.onTick(t));
        summary = sum;
        beginFigureUpdate();
        removeFigure(summaryImprint);
        summaryImprint = createBar(summary);
        addFigure(summaryImprint);
        endFigureUpdate();
      });
    }
  }

  @Override
  public void onDataSeriesMoved(DataContext ctx)
  {
    super.onDataSeriesMoved(ctx);
    // Optimization: Don't recompute the summary if it already exists and its not for the visible bars
    if (!showSummary || (summary != null && !isSpanVisibleBars())) return;
    long start = getSummaryStart(ctx);
    var sum = new SummaryProfile(start, getSummaryEnd(ctx), ctx.getInstrument(), tickInterval);
    var vps = new ArrayList<>(profileMap.values());
    vps.sort((vp1, vp2) -> Long.compare(vp1.getStartTime(), vp2.getStartTime())); // Sort by time in reverse order
    if (Util.isEmpty(vps) || vps.get(vps.size()-1).getStartTime() > start) {
      calculateValues(ctx);
    }
    sum.populate(vps);
    summary = sum;
    var tmp = summaryImprint;
    summaryImprint = createBar(summary);
    removeFigure(tmp);
    addFigure(summaryImprint);
  }

  @Override
  protected void calculateValues(DataContext ctx)
  {
    addFigure(loadingLabel);
    var series = ctx.getDataSeries();
    if (series.size() == 0 || calcInProgress) return;
    // Special case: If we have a summary that displays over the visible bars, we may need to compute more profiles
    if (!profileMap.isEmpty() && showSummary && isSpanVisibleBars()) {
      long start = getSummaryStart(ctx);
      for(var vp : profileMap.values()) {
        if (vp.getStartTime() <= start) return; // enough data available
      }
    }
    else if (!profileMap.isEmpty()) return;

    instr = series.getInstrument();
    tickSize = (float)instr.getTickSize();
    barSize = series.getBarSize();
    splitETH = getSettings().getBoolean(SPLIT_ETH, false) && barSize.getIntervalMinutes() == 1440 && !rthData;

    Util.schedule(() -> calculate(ctx));
  }

  private void calculate(DataContext ctx)
  {
    var series = ctx.getDataSeries();
    var settings = getSettings();
    if (series.size() == 0) return;

    int max = settings.getInteger(IMPRINT_COUNT, 10);
    var showAll = settings.getBoolean(SHOW_ALL, false);
    long summaryStart = getSummaryStart(ctx);

    // Find the starting index
    int limit = showAll ? 0 : series.size()-max-1;
    if (limit < 0 ) limit = 0;
    if (!showAll && showSummary) {
      // Make sure we include enough bars to properly show the summary
      for(int i = limit; i >= 0; i--) {
        if (series.getEndTime(i) <= summaryStart) break;
        limit--;
      }
    }

    int startIndex = limit;
    if (showSummary && isSpanVisibleBars() && !series.isComplete(startIndex)) {}
    else {
      for(int i=series.size()-1; i >= limit; i--) {
        if (series.isComplete(i)) { // already computed
          startIndex = i+1;
          break;
        }
      }
    }

    // Generate a VP for each bar in the series (using the given time frame)
    try {
      calcInProgress = true;
      long start = Util.getStartOfBar(series.getStartTime(startIndex), series.getEndTime(startIndex), instr, series.getBarSize(), rthData);
      calculator = new ProfileCalculator(startIndex, series, rthData);
      instr.forEachTick(start, ctx.getCurrentTime() + 5*Util.MILLIS_IN_MINUTE, ctx.isRTH(), useHistoricalBars, calculator);
    }
    finally {
      calcInProgress=false; // Let the onTick process incoming ticks.  Note: it is possible that we might miss a couple of ticks in a fast moving market!
    }

    updateFigures(ctx);
  }

  private void updateFigures(DataContext ctx)
  {
    var settings = getSettings();
    int max = settings.getInteger(IMPRINT_COUNT, 10);
    var showAll = settings.getBoolean(SHOW_ALL, false);

    var vps = new ArrayList<>(profileMap.values());
    vps.sort((vp1, vp2) -> Long.compare(vp1.getStartTime(), vp2.getStartTime())); // Sort by time
    long summaryStart = 0;
    if (showSummary) {
      summaryStart = getSummaryStart(ctx);
      var sum = new SummaryProfile(summaryStart, getSummaryEnd(ctx), instr, tickInterval);
      if (isCustomMinSummary()) {
        instr.forEachTick(summaryStart, ctx.getCurrentTime() + 5*Util.MILLIS_IN_MINUTE, ctx.isRTH(), useHistoricalBars, tick -> sum.onTick(tick));
      }
      else {
        sum.populate(vps);
      }
      summary = sum;
    }
    else summary = null;

    // Trim profiles
    if (!showAll && vps.size() > max) {
      while(vps.size() > max) {
        var vp = vps.remove(0);
        // Remove the old profiles (if we can)
        if (summaryStart == 0 || vp.getStartTime() < summaryStart) profileMap.remove(vp.getStartTime());
      }
    }

    VolumeProfile prev = null;
    List<Imprint> list = new ArrayList<>();
    for(var vp : vps) {
      if (prev != null) {
        vp.setPrev(prev);
        prev.setNext(vp);
      }
      prev = vp;
      list.add(createBar(vp));
    }

    List<Figure> figures = new ArrayList<>();
    figures.add(loadingLabel);
    if (!Util.isEmpty(vps) && settings.getBoolean(SHOW_TOTALS, false)) {
      var last = vps.get(vps.size()-1);
      // We can't count on the end time here for non-linear bars, so use start time and offset by the bar width when drawing.
      figures.add(new TotalLabel(last.getStartTime()));
    }

    if (showSummary) {
      summaryImprint = createBar(summary);
      figures.add(summaryImprint);
    }

    imprints = list;
    for(var b : list) figures.add(b);
    setFigures(figures);
  }

  private Color getFontColor(Color bg, DrawContext ctx)
  {
    var fc = ctx.getDefaults().getLineColor();
    if (bg == null) return fc;
    int tc = bg.getBlue() + bg.getRed() + bg.getGreen();
    if (tc < 200) return X11Colors.WHITE;
    if (tc > 360) return X11Colors.BLACK;
    return fc;
  }

  protected Imprint createBar(VolumeProfile vp)
  {
    var imp = new Imprint(vp);
    if (barSize.isIntraday()) {
      imp.sod = instr.getStartOfDay(vp.getStartTime(), rthData);
      imp.eod = instr.getEndOfDay(vp.getStartTime(), rthData);
    }
    if (splitETH && ethStarts.contains(vp.getStartTime())) imp.eth=true;
    return imp;
  }

  private Map<Long, VolumeProfile> profileMap = Collections.synchronizedMap(new HashMap());
  private List<Imprint> imprints = Collections.EMPTY_LIST;
  private List<Long> ethStarts = new ArrayList<>();
  private SummaryProfile summary;
  private Imprint summaryImprint;
  private List<ImbalanceSetting> imbSettings;
  private LoadingLabel loadingLabel = new LoadingLabel();
  private boolean calcInProgress=false;

  private ProfileCalculator calculator;

  // Study settings from the Settings object.  See comment above in calculateValues for explanation
  private int absLevel, baFilt, dFilt, vFilt, tickInterval, valueRangeLadder, summaryWidth, summaryOffset, vpPercentRange, maxWidth, dotMinSize, candleWidth;
  private PathInfo baPocBox, dPocBox, vPocBox, bidBox, askBox, nakedPoc, vpRangeLine;
  private ColorInfo bidHist, askHist, valueLadderColor, pocLadderColor, vpRangeFill, posColor, negColor, vpPocColor, vpRangeBarColor,
          pFontColor, lFontColor, baFontColor, dFontColor, vFontColor, totalFontColor;
  private FontInfo lLblFont, lineFont, totalFont, pFont, volFont, bidAskPerFont, posDeltaFont, negDeltaFont, baFont, dFont, vFont, dotBidFont, dotAskFont;
  private boolean baFiltEnabled, dFiltEnabled, vFiltEnabled, rthData, splitETH, baShowFill, dShowFill, vShowFill, showSummary, showVpRange, showOC, showBidAskVol,
          baHist, dHist, vHist, maxWidthEnabled,useHistoricalBars, baLevelsEnabled, autoAdjustBa, totalLevels, baShowImb, dShowImb, vShowImb, lShowImb,
          adjLadderColor, vpShowImb, calcOnClose, showSumTotals, dotsEnabled;
  private String barAlign, baHistAlign, dHistAlign, vHistAlign, baLblAlign, dLblAlign, vLblAlign, type, type2, summaryType, spanType, baLblType, dLblType, vLblType, pLblType, vpAlign;
  private Color askColor, bidColor, vpColor1, vpColor2, vpBarColor, ethBarColor;
  private Color askColorLow, askColorHigh, bidColorLow, bidColorHigh;
  private double askPerLow, askPerHigh, bidPerLow, bidPerHigh;
  private Color posColorLow, posColorHigh, negColorLow, negColorHigh;
  private double posPerLow, posPerHigh, negPerLow, negPerHigh;
  private Rectangle graphBounds;
  private float tickSize;
  private Instrument instr;
  private boolean showTotals, showAVAP, showDeltaPer, showMaxDelta, showMinDelta, showDeltaChange, showDeltaDay, showDelta, showCumDelta, showVolume, showZeros, vpSchemeEnabled, col1Enabled, col2Enabled, useCustSumSpan;
  private BarSize barSize;
  private int custSumSpan=1;
  private String custSumSpanUnits;
  private String baSep, vpScheme, dotOutline;

  // Functional Interface for getting a total value from a VolumeProfile
  static interface TotalFactory { double getValue(VolumeProfile vp); }

  // Functional Interface for formatting a total value
  static interface TotalFormatter { String format(double value); }

  protected class LoadingLabel extends Figure
  {
    Font font = new Font("Arial", Font.PLAIN, 20);
    Color color = Util.awtColor(140, 140, 140, 120);

    @Override
    public boolean contains(double x, double y, DrawContext ctx) { return false; }
    @Override
    public void draw(Graphics2D gc, DrawContext ctx)
    {
      if (!calcInProgress) return;
      gc.setFont(font);
      var gb = ctx.getBounds();
      var fm = gc.getFontMetrics();
      String lbl = get("LBL_VIM_LOADING");
      int w = fm.stringWidth(lbl);
      gc.setColor(color);
      gc.drawString(lbl, gb.x + gb.width/2 - w/2, gb.y + gb.height/2 - fm.getHeight()/2);
    }
  }

  // Encapsulates the Imbalance settings
  private class ImbalanceSetting
  {
    ImbalanceSetting(int level, FontInfo bidFont, FontInfo askFont, PathInfo sl, PathInfo rl, double p, int delta, boolean deltaEnabled)
    {
      this.level = level;
      this.bidFont = bidFont; this.askFont = askFont; per = p; this.delta = delta; this.deltaEnabled = deltaEnabled;
      supportLine = sl; resistanceLine = rl;
    }
    int level;
    FontInfo bidFont, askFont;
    PathInfo supportLine, resistanceLine;
    double per;
    int delta;
    boolean deltaEnabled;
  }

  protected class TotalLabel extends Figure
  {
    TotalLabel(long ts) { labelLeft = ts; }

    @Override
    public void draw(Graphics2D gc, DrawContext ctx)
    {
      var b = graphBounds;
      if (b == null) return;
      int x = ctx.translateTime(labelLeft) + 5;
      if (barAlign == MIDDLE) x += ctx.getBarWidth()/2;
      else x += ctx.getBarWidth();
      if (x > b.x + b.width || x < b.x - ctx.getBarWidth()) return;

      gc.setFont(totalFont.getFont());
      gc.setColor(ctx.getDefaults().getLineColor());
      if (totalFontColor != null && totalFontColor.isEnabled()) gc.setColor(totalFontColor.getColor());
      var fm = gc.getFontMetrics();
      int h = fm.getAscent() + 3;
      int y = b.y + b.height - h;

      if (showAVAP) { gc.drawString(get("LBL_AVAP"), x, y + h - 2); y -= h; }
      if (showDeltaPer) { gc.drawString(get("LBL_DELTA_PER"), x, y + h - 2); y -= h; }
      if (showMaxDelta) { gc.drawString(get("LBL_MAX_DELTA"), x, y + h - 2); y -= h; }
      if (showMinDelta) { gc.drawString(get("LBL_MIN_DELTA"), x, y + h - 2); y -= h; }
      if (showDeltaChange) { gc.drawString(get("LBL_DELTA_CHANGE"), x, y + h - 2); y -= h; }
      if (showDeltaDay) { gc.drawString(get("LBL_DELTA_DAY"), x, y + h - 2); y -= h; }
      if (showDelta) { gc.drawString(get("LBL_DELTA"), x, y + h - 2); y -= h; }
      if (showCumDelta) { gc.drawString(get("LBL_CUM_DELTA"), x, y + h - 2); y -= h; }
      if (showVolume) gc.drawString(get("LBL_VOLUME"), x, y + h - 2);
    }

    long labelLeft=0;
  }

  // Displays the Volume Imprint for a specific bar
  protected class Imprint extends Figure
  {
    protected Imprint(VolumeProfile vp) { this.vp = vp; }

    @Override
    public boolean contains(double x, double y, DrawContext ctx)
    {
      if (!col1Enabled && !vp.isSummary()) return false;
      for(var b : bars) {
        if (b.contains(x, y)) return true;
      }
      if (!Util.isEmpty(bars2)) {
        for(var b : bars2) {
          if (b.contains(x, y)) return true;
        }
      }
      if (ocRange != null && ocRange.contains(x, y)) return true;
      if (bidAskRange != null && bidAskRange.contains(x, y)) return true;
      return false;
    }

    @Override
    public String getPopupMessage(double x, double y, DrawContext ctx)
    {
      if (!col1Enabled && !vp.isSummary()) return null;

      if (ocRange != null && ocRange.contains(x, y)) {
        String str = get("LBL_OPEN_PRICE") + ": " + instr.format(vp.getOpenPrice()) + " \n";
        str += get("LBL_HIGH_PRICE") + ": " + instr.format(vp.getHighPrice()) + " \n";
        str += get("LBL_LOW_PRICE") + ": " + instr.format(vp.getLowPrice()) + " \n";
        str += get("LBL_CLOSE_PRICE") + ": " + instr.format(vp.getClosePrice()) + " \n";
        return str;
      }

      if (bidAskRange != null && bidAskRange.contains(x, y)) {
        String str = get("LBL_VOLUME") + ": " + Util.formatMK(vp.getTotalVolume()) + "\n";
        if (vp.getTotalBidVolume() > 0) str += get("LBL_BID_VOLUME") + ": " + Util.formatMK(vp.getTotalBidVolume()) + "\n";
        if (vp.getTotalAskVolume() > 0) str += get("LBL_ASK_VOLUME") + ": " + Util.formatMK(vp.getTotalAskVolume()) + "\n";
        if (vp.getTotalDelta() != 0) str += get("LBL_DELTA") + ": " + Util.formatMK(vp.getTotalDelta()) + "\n";
        return str;
      }

      for(var b : bars) {
        if (!b.contains(x, y)) continue;
        return getPopupMsg(b.row);
      }
      if (!Util.isEmpty(bars2)) {
        for(var b : bars2) {
          if (!b.contains(x, y)) continue;
          return getPopupMsg(b.row);
        }
      }
      return null;
    }

    private String getPopupMsg(VolumeRow row)
    {
      var str = get("LBL_PRICE_RANGE") + ": " + instr.format(row.getStartPrice()) + " - " + instr.format(row.getEndPrice()) + "\n";
      str += get("LBL_VOLUME") + ": " + Util.formatMK(row.getVolume()) + "\n";
      if (row.getBidVolume() > 0) str += get("LBL_BID_VOLUME") + ": " + Util.formatMK(row.getBidVolume()) + "\n";
      if (row.getAskVolume() > 0) str += get("LBL_ASK_VOLUME") + ": " + Util.formatMK(row.getAskVolume()) + "\n";
      if (row.getDelta() != 0) str += get("LBL_DELTA") + ": " + Util.formatMK(row.getDelta()) + "\n";
      if (vp.getTotalVolume() <= 0) return str;
      str += "-----------------------\n";
      if (vp.getTotalVolume() > 0) str += get("LBL_TOTAL_VOLUME") + ": " + Util.formatMK(vp.getTotalVolume()) + "\n";
      if (vp.getTotalAskVolume() > 0) str += get("LBL_TOTAL_ASK_VOLUME") + ": " + Util.formatMK(vp.getTotalAskVolume()) + "\n";
      if (vp.getTotalBidVolume() > 0) str += get("LBL_TOTAL_BID_VOLUME") + ": " + Util.formatMK(vp.getTotalBidVolume()) + "\n";
      if (vp.getTotalDelta() != 0) str += get("LBL_TOTAL_DELTA") + ": " + Util.formatMK(vp.getTotalDelta()) + "\n";
      str += get("LBL_TICKS") + ": " + vp.getTicks() + "\n";
      return str;
    }

    private boolean isAlwaysVisible() { return alwaysVisible != null && alwaysVisible; }

    @Override
    public boolean isVisible(DrawContext ctx)
    {
      if (vp == null || ctx.getDataContext() == null) return false;
      if (isAlwaysVisible() || vp.isSummary()) return true;

      // Hack: the layout method will confirm if an imprint is always visible
      // That needs to be called in order to determine its status.  Return true to ensure that layout gets called.
      if (alwaysVisible == null) return true;

      // This should be folded into the check for always visible
      if (nakedPoc != null && nakedPoc.isEnabled())  return true;

      //if (nprx > 0) return true; // nprx is the naked poc line
      var series = ctx.getDataContext().getDataSeries();
      if (vp.getEndTime() < series.getVisibleStartTime() || vp.getStartTime() > series.getVisibleEndTime()) return false;
      return true;
    }

    @Override
    public void layout(DrawContext ctx)
    {
      graphBounds = ctx.getBounds();
      String t = type;
      String t2 = type2;
      if (vp.isSummary()) { t = summaryType; t2 = null; }
      boolean ladder = Util.compare(t, LADDER) || Util.compare(t2, LADDER);
      lx = ctx.translateTime(vp.getStartTime());
      if (barAlign == MIDDLE && !splitETH) lx -= ctx.getBarWidth()/2; // Center around the candle
      rx = lx + ctx.getBarWidth();
      // Hack: if the bar size is different from the chart bar size, we cannot use the bar width
      // Instead, we need to use the end time for the profile
      var cbs = ctx.getDataContext().getChartBarSize();
      var bs = ctx.getDataContext().getDataSeries().getBarSize();
      if (!Util.compare(cbs, bs) || splitETH) {
        lx = ctx.translateTime(vp.getStartTime());
        rx = ctx.translateTime(vp.getEndTime());
        int w = rx - lx;
        if (!splitETH) {
          if (barAlign == MIDDLE) {
            lx -= w/2;
            rx -= w/2;
          }
          else if (barAlign == END && maxWidthEnabled) {
            if (rx - maxWidth > lx) lx = rx - maxWidth;
          }
        }
        // If this is the last profile, limit the right side to the graph bounds
        if (bs.getSizeMillis() > cbs.getSizeMillis() && vp.getEndTime() > ctx.getDataContext().getCurrentTime()) {
          if (rx > graphBounds.getMaxX()) rx = (int)graphBounds.getMaxX();
        }
      }
      if (vp.isSummary()) { // summary is always on the right side
        rx = graphBounds.x + graphBounds.width - summaryOffset;
        lx = rx - summaryWidth;
      }

      int _lx = lx;
      int _rx = rx;

      int oy = ctx.translateValue(vp.getOpenPrice());
      int cy = ctx.translateValue(vp.getClosePrice());
      int hy = ctx.translateValue(vp.getHighPrice());
      int ly = ctx.translateValue(vp.getLowPrice());

      boolean rangeRight = (Util.in(t, PROFILE, DELTA) || Util.in(t2, PROFILE, DELTA)) && (vp.isSummary() || vpAlign != LEFT);
      boolean _showOC = showOC & !ladder, _showBidAskVol = showBidAskVol && !ladder;
      if (_showOC) {
        if (rangeRight) { _lx += 2; _rx -= 6; }
        else { _lx += 5; _rx -= 2; }
      }
      if (_showBidAskVol) {
        if (rangeRight) _rx -= 6;
        else { _lx += 4; _rx -= 3; }
      }

      List<Bar> _bars = null;
      int maxw = vp.isSummary() ? summaryWidth : _rx - _lx - 3;
      int bw = ctx.getBarWidth();
      boolean baWidthCalculated = false;  // TODO: we may not need to calculate the BA width.  Should be checking the label types to see if its not used
      // Optimization: check to see if we need to calculate the BA width
      if (dLblType != BID_ASK && vLblType != BID_ASK && pLblType != BID_ASK) baWidthCalculated = true;

      if (!vp.isSummary() && maxWidthEnabled && maxWidth < maxw) maxw = maxWidth;
      if (vp.isSummary()) { // Only show the first column for the summary (for now)
        int x = _lx;
        if (Util.in(t, DELTA, PROFILE)) {
          if (_showOC) x -= 7;
          if (_showBidAskVol) x -= 8;
        }
        bars = createBars(ctx, t, x, maxw, false);
        _bars = bars;
        bars2 = null;
      }
      else {
        if (col2Enabled && col1Enabled) { // Two Columns
          int minw1 = 0;
          for(var row : vp.getRows()) {
            int w = calcWidth(type, row, bw);
            if (w > minw1) minw1 = w;
          }
          if (type == BID_ASK) { baWidthCalculated = true; maxBAWidth = minw1; }
          int minw2 = 0;
          for(var row : vp.getRows()) {
            int w = calcWidth(type2, row, bw);
            if (w > minw2) minw2 = w;
          }
          if (type2 == BID_ASK) { baWidthCalculated = true; maxBAWidth = minw2; }

          // Distribute the widths from the max width for the bar
          if (minw1 == 0 && minw2 == 0) { minw1 = minw2 = maxw/2; }
          else if (minw1 == 0) { // use the remainder
            minw1 = maxw - minw2;
            if (minw1 < 50) minw1 = 50; // need a reasonable width here.  Is this enough?
          }
          else if (minw2 == 0) { // use the remainder
            minw2 = maxw - minw1;
            if (minw2 < 50) minw2 = 50; // need a reasonable width here.  Is this enough?
          }
          if (barAlign == MIDDLE && !splitETH) _lx = lx + (rx-lx)/2 - minw1/2 - minw2/2; // centered around the price bar
          else _lx = lx;
          _rx += 3;

          bars = createBars(ctx, type, _lx, minw1, false);
          _bars = createBars(ctx, type2, _lx+minw1+1, minw2-1, true);
          bars2 = _bars;
          for(int i=0; i < bars.size(); i++) {
            bars.get(i).bar2 = bars2.get(i);
          }
        }
        else if (col1Enabled) {
          int minw = 0;
          if (maxWidthEnabled) minw = maxw;
          else {
            for(var row : vp.getRows()) {
              int w = calcWidth(type, row, bw);
              if (w > minw) minw = w;
            }
            if (type == BID_ASK) { baWidthCalculated = true; maxBAWidth = minw; }
          }
          if (minw == 0) minw = maxw;

          if (barAlign == MIDDLE && !splitETH) _lx = lx + (rx-lx)/2 - minw/2; // centered around the price bar
          else _lx = lx;
          _bars = createBars(ctx, type, _lx, minw, false);
          _rx = _lx + minw;
          bars = _bars;
          bars2 = null;
        }
        else {
          bars = bars2 = null;
          return;
        }
      }

      // We need the max width of the BidAsk Column if we are going to align it.
      // Calculate it here (if necessary)
      if (!baWidthCalculated) {
        maxBAWidth = 0;
        for(var row : vp.getRows()) {
          int w = calcBAWidth(row);
          if (w > maxBAWidth) maxBAWidth = w;
        }
      }

      ocRange = bidAskRange = null;
      int x1 = _lx-5, x2 = _rx+2;
      if (_showBidAskVol) {
        bidAskRange = rangeRight ? new Rectangle(x2, hy, 4, ly - hy) : new Rectangle(x1-1, hy, 4, ly - hy);
        x1 -= 6;
        x2 += 6;
      }
      if (_showOC) ocRange = rangeRight ? new Rectangle(x2, Math.min(oy, cy), 3, Math.abs(cy-oy)) : new Rectangle(x1, Math.min(oy, cy), 3, Math.abs(cy-oy));

      // Draw the Naked POC Line (for the last column only)
      nplx = nprx = -1;
      var _imprints = imprints;
      if (!Util.isEmpty(_imprints) && _bars != null) {
        var latest = _imprints.get(_imprints.size()-1);
        if (nakedPoc != null && nakedPoc.isEnabled() && latest != this && !vp.isSummary()) {
          x1 =-1; x2=-1;
          for(int i=0; i < _bars.size()-1; i++) {
            var b = _bars.get(i);
            if (!b.row.isPOC()) continue;
            npy = b.area.y + b.area.height/2;
            x1 = b.getRightX();
            x2 = ctx.translateTime(latest.vp.getEndTime());
            int ind = getIndex();
            if (ind < 0) break;
            for(int j = ind+1; j < _imprints.size(); j++) {
              var imp = _imprints.get(j);
              if (b.row.getStartPrice() > imp.vp.getHighPrice() || b.row.getEndPrice() <= imp.vp.getLowPrice()) continue;
              if (j == ind+1) {
                x1 = x2 = -1;
                break; // Don't bother drawing the npoc line if it ends at the next bar.
              }
              if (!Util.isEmpty(imp.bars)) x2 = imp.bars.get(0).area.x;
              else x2 = ctx.translateTime(imp.vp.getStartTime());
              if (ladder) x2 -= (rx-lx)/2; // HACK: for the ladder, move it back a bit

              break;
            }
            break;
          }
          nplx = x1;
          nprx = x2;
        }
      }

      // Check if this imprint has extended lines (and must be drawn when off to the left of the screen)
      alwaysVisible=false;
      for(Bar b : bars) {
        if (b.isAlwaysVisible(ctx)) { alwaysVisible = true; break; }
      }
      if (!alwaysVisible && !Util.isEmpty(bars2)) {
        for(Bar b : bars2) {
          if (b.isAlwaysVisible(ctx)) { alwaysVisible = true; break; }
        }
      }
    }

    List<Bar> createBars(DrawContext ctx, String t, int _lx, int minw, boolean col2)
    {
      int th = (int)Math.round(ctx.getTickHeight()/2);
      if (th <= 0) th = 1;
      var vpoc = vp.getPOC();
      double maxVol = vpoc == null ? 0 : vpoc.getVolume();
      var poc = vpPocColor;
      boolean vpl = vpAlign == LEFT;
      if (vp.isSummary()) vpl = false;

      var _bars = new ArrayList<Bar>();
      for(var row : vp.getRows()) {
        int sp = ctx.translateValue(row.getStartPrice());
        int ep = ctx.translateValue(row.getEndPrice());
        int height = Math.abs(sp - ep);
        if (height < 2) height = 2;
        int y = Math.min(sp, ep); // the end price should have a lower y value than the start price unless the chart is inverted
        y += th;
        Bar bar = null;
        var area = new Rectangle(_lx, y, minw, height);
        switch(t) {
          case VOLUME: bar = new VolumeBar(row, area); break;
          case BID_ASK: bar = new BidAskBar(row, area); break;
          case LADDER: bar = new LadderBar(row, area); break;
          case DELTA: bar = new DeltaBar(row, area); break;
          default:
            int mw = dotsEnabled ? minw - 15 : minw;
            int width = (int)((row.getVolume()/maxVol) * mw);
            bar = new ProfileBar(row, area, new Rectangle(vpl ? _lx : _lx + mw-width, y, Math.max(width, 2), height));
            if (poc.isEnabled() && row.isPOC()) ((ProfileBar)bar).barColor = poc.getColor();
            break;
        }
        _bars.add(bar);
      }

      if (showVpRange) {
        double per = vpPercentRange/100.0;
        int inds[] = vp.getValueArea(per);
        if (inds != null) {
          vpRangeTopInd = inds[1];
          vpRangeBottomInd = inds[0];
          if (vpRangeBarColor.isEnabled()) {
            for(int i=inds[0]; i <= inds[1]; i++) {
              if (i < 0) continue;
              if (poc.isEnabled() && _bars.get(i).row.isPOC()) continue;
              Bar bar = _bars.get(i);
              if (!(bar instanceof ProfileBar)) continue;
              ((ProfileBar)bar).barColor = vpRangeBarColor.getColor();
            }
          }
        }
      }
      return _bars;
    }

    int calcWidth(String t, VolumeRow row, int bw)
    {
      switch(t) {
        case VOLUME:
          return Util.strWidth(Util.formatMK(row.getVolume()), vFont.getFont()) + 10;
        case BID_ASK:
          return calcBAWidth(row);
        case LADDER:
          var fm = Util.getFontMetrics(lLblFont.getFont());
          return Math.max(fm.stringWidth(Util.formatMK(row.getBidVolume())), fm.stringWidth(Util.formatMK(row.getAskVolume())))*2 + 16 + getLadderCenterWidth(bw);
        case DELTA:
          var d = Util.formatMK(row.getDelta());
          if (d.length() < 3) d = "XXX";
          return Util.strWidth(d, dFont.getFont()) + 10;
      }
      return -1;
    }

    private int calcBAWidth(VolumeRow row)
    {
      var fm = Util.getFontMetrics(baFont.getFont());
      return fm.stringWidth(baSep) + Math.max(fm.stringWidth(Util.formatMK(row.getBidVolume())), fm.stringWidth(Util.formatMK(row.getAskVolume())))*2 + 16;
    }

    // HACK: hard code 18 for now...
    int getLadderCenterWidth(int bw) { return candleWidth+4; }

    @Override
    public void draw(Graphics2D gc, DrawContext ctx)
    {
      int w = rx - lx;
      if (lx < -w && nprx < 0 && !isAlwaysVisible()) return;
      var _bars = bars;
      var t = vp.isSummary() ? summaryType : type;

      if (_bars != null) {
        // Draw Open/Close and/or Bid/Ask Ranges
        var r = ocRange;
        if (r != null) {
          if (vp.getClosePrice() > vp.getOpenPrice()) gc.setColor(ctx.getDefaults().getBarUpColor());
          else if (vp.getClosePrice() < vp.getOpenPrice()) gc.setColor(ctx.getDefaults().getBarDownColor());
          else gc.setColor(ctx.getDefaults().getBarNeutralColor());
          gc.fill(r);
        }
        r = bidAskRange;
        if (r != null) {
          int askHeight = (int)(r.height * (vp.getTotalAskVolume() / vp.getTotalVolume()));
          gc.setColor(vpColor2);
          gc.fillRect(r.x, r.y, r.width, askHeight);
          gc.setColor(vpColor1);
          gc.fillRect(r.x, r.y+askHeight, r.width, r.height - askHeight);
        }

        if (Util.compare(t, LADDER)) drawLadder(gc, ctx, _bars);
        else if (Util.compare(t, PROFILE)) drawProfile(gc, ctx, _bars);
        else {
          // Bid/Ask, Volume, Delta Bars
          for(Bar b : _bars) {
            if (b.intersects(ctx)) b.draw(gc, ctx);
          }
        }

        var _bars2 = bars2;
        if (!Util.isEmpty(_bars2)) {
          if (Util.compare(type2, LADDER)) drawLadder(gc, ctx, _bars2);
          else if (Util.compare(type2, PROFILE)) drawProfile(gc, ctx, _bars2);
          else {
            // Bid/Ask, Volume, Delta Bars
            for(Bar b : _bars2) {
              if (b.intersects(ctx)) b.draw(gc, ctx);
            }
          }
        }

        // Draw Trade Dots
        if (dotsEnabled) {
          var _b = _bars.get(0);
          int _lx = _b.area.x;
          if (ocRange != null) _lx = ocRange.x;
          if (_bars2 != null) _b = _bars2.get(0);

          int _rx = _b.area.x + _b.area.width;
          if (_b.isProfile()) {
            _rx -= 15;
            if (_rx < _lx) _rx = _lx;
          }
          _lx -= 2;
          _rx += 2;

          // Draw trades
          var bfm = Util.getFontMetrics(dotBidFont.getFont());
          var afm = Util.getFontMetrics(dotAskFont.getFont());

          int maxSize = 0;
          for(var b : _bars) {
            if (b.row.getBidTrades().size() > maxSize) maxSize = b.row.getBidTrades().size();
            if (b.row.getAskTrades().size() > maxSize) maxSize = b.row.getAskTrades().size();
          }

          if (maxSize > 0) {
            int bidMaxWidth = bfm.stringWidth(Util.formatMK(maxSize));
            int askMaxWidth = afm.stringWidth(Util.formatMK(maxSize));

            for(var b : _bars) {
              var bids = b.row.getBidTrades();
              if (!bids.isEmpty()) {
                String lbl = Util.formatMK(bids.size());
                int width = bfm.stringWidth(lbl);
                int height = bfm.getHeight();
                b.drawTradeDot(gc, dotBidFont, lbl, bids.size(), maxSize, _lx-(bidMaxWidth - width)/2 - width-4, b.getMidY() - height/2, width, height);
              }
              var asks = b.row.getAskTrades();
              if (!asks.isEmpty()) {
                String lbl = Util.formatMK(asks.size());
                int width = afm.stringWidth(lbl);
                int height = afm.getHeight();
                b.drawTradeDot(gc, dotAskFont, lbl, asks.size(), maxSize, _rx+4 + (askMaxWidth - width)/2, b.getMidY() - height/2, width, height);
              }
            }
          }
        }

        // Draw the Naked POC Line (if applicable)
        if (nakedPoc != null && nakedPoc.isEnabled() && nprx > 0) {
          gc.setStroke(nakedPoc.getStroke());
          gc.setColor(nakedPoc.getColor());
          int x1 = nplx, x2 = nprx, y = npy;
          if (x1 < 0) x1 = 0;
          if (lineFont.isEnabled()) {
            float price = instr.round((vp.getPOC().getStartPrice() + vp.getPOC().getEndPrice())/2 - tickSize/2);
            var str = instr.format(price);
            gc.setFont(lineFont.getFont());
            var fm = gc.getFontMetrics();
            int sw = fm.stringWidth(str);
            gc.drawString(str, x2 - sw, y + fm.getHeight()/2 - 2);
            x2 -= sw + 2;
          }
          if (x1 < x2) gc.drawLine(x1, y, x2, y);
        }
      }

      drawCandleTotals(gc, ctx);
      drawTotals(gc, ctx);
    }

    void drawLadder(Graphics2D gc, DrawContext ctx, List<Bar> _bars)
    {
      // Draw the price bar in the center
      if (Util.isEmpty(_bars)) return;
      int cx = _bars.get(0).area.x + _bars.get(0).area.width/2;
      if (vp.getClosePrice() > vp.getOpenPrice()) gc.setColor(ctx.getDefaults().getBarUpColor());
      else if (vp.getClosePrice() < vp.getOpenPrice()) gc.setColor(ctx.getDefaults().getBarDownColor());
      else gc.setColor(ctx.getDefaults().getBarNeutralColor());

      int hy = ctx.translateValue(vp.getHighPrice());
      int cy = ctx.translateValue(vp.getClosePrice());
      int oy = ctx.translateValue(vp.getOpenPrice());
      int ly = ctx.translateValue(vp.getLowPrice());
      gc.setStroke(STROKE2);
      gc.drawLine(cx, hy, cx, Math.min(cy, oy)-1);
      gc.drawLine(cx, Math.max(cy, oy), cx, ly);
      int w = getLadderCenterWidth(ctx.getBarWidth()) - 4;
      gc.fillRect(cx-w/2, Math.min(cy, oy), w, Math.abs(cy-oy));
      gc.setColor(gc.getColor().darker());
      gc.setStroke(STROKE1);
      gc.drawRect(cx-w/2, Math.min(cy, oy), w-1, Math.abs(cy-oy)-1);

      if (valueLadderColor != null && valueLadderColor.isEnabled()) {
        int range[] = vp.getValueArea(valueRangeLadder/100d);
        if (range != null && range.length == 2) {
          gc.setColor(valueLadderColor.getColor());
          int ind0 = range[0], ind1=range[1];
          if (ind0 >= 0 && ind0 < _bars.size() && ind1 >=0 && ind1 < _bars.size()) {
            int y = _bars.get(range[1]).area.y;
            int h = (int)_bars.get(range[0]).area.getMaxY() - y;
            gc.fillRect(cx - w/2 + 2, y, w-4, h);
          }
        }
      }

      for(var b : _bars) {
        if (b.intersects(ctx)) b.draw(gc, ctx);
        if (pocLadderColor != null && pocLadderColor.isEnabled() && b.row.isPOC()) {
          gc.setColor(pocLadderColor.getColor());
          gc.fillRect(cx - w/2 + 2, b.area.y, w-4, b.area.height);
        }
      }
    }

    void drawProfile(Graphics2D gc, DrawContext ctx, List<Bar> _bars)
    {
      var showValueArea = showVpRange && vpRangeTopInd >=0 && vpRangeBottomInd >= 0;
      if (showValueArea && vpRangeFill.isEnabled()) {
        var top = _bars.get(vpRangeTopInd);
        var bottom = _bars.get(vpRangeBottomInd);
        int ty = top.area.y, by = bottom.area.y + bottom.area.height, x = top.area.x, w = top.area.width;
        gc.setColor(vpRangeFill.getColor());
        gc.fillRect(x, ty, w, by - ty);
      }
      for(var b : _bars) {
        if (b.intersects(ctx)) b.draw(gc, ctx);
      }

      if (showValueArea) {
        var top = _bars.get(vpRangeTopInd);
        var bottom = _bars.get(vpRangeBottomInd);
        drawLine(gc, ctx, vpRangeLine, top.row.getEndPrice(), top.area.x, top.area.x + top.area.width, -1);
        drawLine(gc, ctx, vpRangeLine, bottom.row.getStartPrice(), top.area.x, top.area.x + top.area.width, -1);
      }
    }

    protected void drawLine(Graphics2D gc, DrawContext ctx, PathInfo path, float price, int _lx, int _rx, int yoffset)
    {
      if (!path.isEnabled()) return;
      int y = ctx.translateValue(price) + (int)Math.round(ctx.getTickHeight()/2) + yoffset;
      if (ctx.isSelected()) gc.setStroke(path.getSelectedStroke());
      else gc.setStroke(path.getStroke());
      gc.setColor(path.getColor());

      if (lineFont.isEnabled()) {
        var str = instr.format(price);
        gc.setFont(lineFont.getFont());
        var fm = gc.getFontMetrics();
        if (vpAlign == LEFT) {
          int x = _rx - fm.stringWidth(str) - 2;
          gc.drawString(str, x, y + fm.getHeight()/2 - 2);
          _rx = x - 4;
        }
        else {
          gc.drawString(str, _lx, y + fm.getHeight()/2 - 2);
          _lx += fm.stringWidth(str) + 2;
        }
      }
      if (_lx < _rx) gc.drawLine(_lx, y, _rx, y);
    }

    // Candle totals
    void drawCandleTotals(Graphics2D gc, DrawContext ctx)
    {
      var _bars = bars;
      if (Util.isEmpty(_bars)) return;

      var top = vp.getClosePrice() > vp.getOpenPrice();
      if (vp.getClosePrice() == vp.getOpenPrice()) top = vp.getTotalDelta() >= 0;
      if (vp.isSummary()) top = false; // always show totals at the bottom for the summary
      int y = top ? _bars.get(_bars.size()-1).area.y - 6 : (int)_bars.get(0).area.getMaxY() + 2;
      int llx = _bars.get(0).area.x;
      int rrx = llx + _bars.get(0).area.width;

      var deltaFont = vp.getTotalDelta() >= 0 ? posDeltaFont : negDeltaFont;
      if (bidAskPerFont != null && bidAskPerFont.isEnabled()) {
        gc.setFont(bidAskPerFont.getFont());
        gc.setColor(bidAskPerFont.getColor());
        var fm = gc.getFontMetrics();
        if (!top) y += fm.getAscent() + 2;
        var bidPer = Util.formatDouble( (vp.getTotalBidVolume() / vp.getTotalVolume())*100, 2);
        var askPer = Util.formatDouble( (vp.getTotalAskVolume() / vp.getTotalVolume())*100, 2);
        gc.drawString(bidPer, llx, y);
        int bw = fm.stringWidth(bidPer);
        int _rx = rrx - fm.stringWidth(askPer);
        if (_rx < llx + bw + 8) _rx = llx + bw + 8;
        gc.drawString(askPer, _rx, y);
        if (top) y -= (fm.getAscent() + 2);
      }

      if (volFont != null && volFont.isEnabled()) {
        gc.setFont(volFont.getFont());
        gc.setColor(volFont.getColor());
        var fm = gc.getFontMetrics();
        if (!top) y += fm.getAscent() + 2;
        var str = Util.formatMK(vp.getTotalVolume());
        gc.drawString(str, (llx + rrx)/2 - fm.stringWidth(str)/2, y);
        if (top) y -= (fm.getAscent() + 2);
      }

      if (deltaFont != null && deltaFont.isEnabled()) {
        gc.setFont(deltaFont.getFont());
        gc.setColor(deltaFont.getColor());
        var fm = gc.getFontMetrics();
        if (!top) y += fm.getAscent() + 2;
        var str = Util.formatMK(vp.getTotalDelta());
        gc.drawString(str, (llx + rrx)/2 - fm.stringWidth(str)/2, y);
        if (top) y -= (fm.getAscent() + 2);
      }
    }

    void drawTotals(Graphics2D gc, DrawContext ctx)
    {
      if (!showTotals || (vp.isSummary() && !showSumTotals)) return;

      gc.setFont(totalFont.getFont());
      gc.setColor(X11Colors.WHITE);
      var fm = gc.getFontMetrics();
      int h = fm.getAscent() + 3;
      int y = graphBounds.y + graphBounds.height - h;
      if (showAVAP) { drawBox(gc, ctx, v -> v.getAVAP(), y, h); y -= h; }
      if (showDeltaPer) {
        double per = vp.getDeltaPer();
        int w = ctx.getBarWidth() - 1; // adjust depending on how much room there is
        String str = Util.formatDouble(per *100, 2) + "%";
        if (fm.stringWidth(str) > w) str = Util.formatDouble(per *100, 2);
        if (fm.stringWidth(str) > w) str = Util.formatDouble(per *100, 1);
        String s = str;
        drawBox(gc, ctx, v -> v.getDeltaPer(), v -> s, y, h, true);
        y -= h;
      }
      if (showMaxDelta) { drawBox(gc, ctx, v -> v.getMaxDelta(), y, h); y -= h; }
      if (showMinDelta) { drawBox(gc, ctx, v -> v.getMinDelta(), y, h); y -= h; }
      if (showDeltaChange) { drawBox(gc, ctx, v -> v.getDeltaChange(), y, h); y -= h; }
      if (showDeltaDay) { drawBox(gc, ctx, v -> v.getTotalDelta(), y, h); y -= h; }
      if (showDelta) { drawBox(gc, ctx, v -> v.getTotalDelta(), y, h); y -= h; }
      if (showCumDelta) {
        drawBox(gc, ctx, v -> {
          double d = v.getTotalDelta();
          var p = v.getPrev();
          while(p != null && p.getStartTime() >= sod) {
            d += p.getTotalDelta();
            p = p.getPrev();
          }
          return d;
        }, val -> Util.formatMK(val), y, h, false);
        y -= h;
      }
      if (showVolume) drawBox(gc, ctx, v -> v.getTotalVolume(), y, h);
    }

    void drawBox(Graphics2D gc, DrawContext ctx, TotalFactory valFact, int y, int height)
    {
      drawBox(gc, ctx, valFact, val -> Util.formatMK(val), y, height, true);
    }

    void drawBox(Graphics2D gc, DrawContext ctx, TotalFactory valFact, TotalFormatter formatter, int y, int height, boolean supportsLevels)
    {
      double val = valFact.getValue(vp);
      int x = lx+1, w = rx-lx-1;
      Color bg = null;

      if (val >= 0 && posColor.isEnabled()) {
        bg = posColor.getColor();
        if (supportsLevels && totalLevels && barSize.isIntraday()) {
          double max = getMaxValue(valFact); // consider caching this value if performance is an issue
          if (val/max >= posPerHigh) bg = posColorHigh;
          else if (val/max <= posPerLow) bg = posColorLow;
        }
      }
      else if (val < 0 && negColor.isEnabled()) {
        bg = negColor.getColor();
        if (supportsLevels && totalLevels && barSize.isIntraday()) {
          double min = getMinValue(valFact); // consider caching this value if performance is an issue
          if (val/min >= negPerHigh) bg = negColorHigh;
          else if (val/min <= negPerLow) bg = negColorLow;
        }
      }

      gc.setColor(bg);
      gc.fillRect(x, y, w, height-1);
      gc.setColor(getFontColor(bg, ctx));
      if (totalFontColor != null && totalFontColor.isEnabled()) gc.setColor(totalFontColor.getColor());
      String str = formatter.format(val);
      gc.drawString(str, x + w/2 - gc.getFontMetrics().stringWidth(str)/2, y + height - 2);
    }

    // Gets the minimum value for the day range
    double getMinValue(TotalFactory valFact)
    {
      double min = valFact.getValue(vp);
      var v = vp.getPrev();
      while(v != null && v.getStartTime() >= sod) {
        double value = valFact.getValue(v);
        if (value < min) min = value;
        v = v.getPrev();
      }
      v = vp.getNext();
      while(v != null && v.getStartTime() < eod) {
        double value = valFact.getValue(v);
        if (value < min) min = value;
        v = v.getNext();
      }
      return min;
    }

    // Gets the maximum value for the day range
    double getMaxValue(TotalFactory valFact)
    {
      double max = valFact.getValue(vp);
      var v = vp.getPrev();
      while(v != null && v.getStartTime() >= sod) {
        double value = valFact.getValue(v);
        if (value > max) max = value;
        v = v.getPrev();
      }
      v = vp.getNext();
      while(v != null && v.getStartTime() < eod) {
        double value = valFact.getValue(v);
        if (value > max) max = value;
        v = v.getNext();
      }
      return max;
    }

    // Optimization: only compute the index if the imprints array has changed
    private int getIndex()
    {
      if (lastImprints == imprints && index >= 0) return index;
      lastImprints = imprints;
      if (Util.isEmpty(lastImprints)) index = -1;
      else {
        for(int i = 0; i < lastImprints.size(); i++) {
          var imp = lastImprints.get(i);
          if (imp == this || imp.vp.getStartTime() == vp.getStartTime()) {
            index = i;
            return index;
          }
        }
      }
      index = -1;
      return index;
    }

    private int index = -1;
    private List<Imprint> lastImprints;

    protected VolumeProfile vp;
    private Rectangle ocRange, bidAskRange;
    private List<Bar> bars = new ArrayList<>(); // Column 1
    private List<Bar> bars2 = new ArrayList<>(); // Column 2
    private int vpRangeTopInd=-1, vpRangeBottomInd=-1;
    protected int lx, rx;
    private int nplx, nprx, npy; // Naked POC left/right
    private int maxBAWidth; // Hack: this is used to align the Bid/Ask Labels
    private boolean eth=false;
    private Boolean alwaysVisible = null;
    long sod, eod; // Start/End of Day (intraday data only)

    class Bar
    {
      Bar(VolumeRow row, Rectangle area)
      {
        this.row = row;
        this.area = area;
        if (area.height < 2) area.height = 2;
      }

      PathInfo getPOCBox() { return null; }
      boolean isShowImb() { return true; }
      boolean isShowFill() { return true; }
      String getHistAlign() { return LEFT; }
      String getLblAlign() { return RIGHT; }
      boolean isHistogram() { return vp.isSummary(); }
      String getLblType() { return null; }
      FontInfo getLblFont() { return null; }
      ColorInfo getLblColor() { return null; }
      boolean isProfile() { return false; } // Hack: for some of the methods we do it differently for a Profile bar

      int getMid() { return area.x + area.width/2; }

      int getMidY() { return area.y + area.height/2; }

      boolean intersects(DrawContext ctx)
      {
        if (area.y + area.height < graphBounds.y || area.y > graphBounds.y + graphBounds.height) return false;
        if (alwaysVisibleSaved != null && !alwaysVisibleSaved) {
          if (area.x + area.width + 10 < graphBounds.x) return false;
          if (area.x + 10 > graphBounds.x + graphBounds.width) return false;
        }
        return true;
      }

      void draw(Graphics2D gc, DrawContext ctx)
      {
        drawSRLines(gc, ctx);
        var bg = drawFill(gc, ctx);
        drawLabel(gc, ctx, bg);
        drawOutlines(gc, ctx);
      }

      void drawLabel(Graphics2D gc, DrawContext ctx, Color bg)
      {
        var b = bar == null ? area : bar;
        if (b.height <= 5) return;
        var f = getLblFont();
        if (!f.isEnabled()) return;
        var c = getLblColor();
        FontMetrics fm;
        String align = getLblAlign();
        boolean left = align == LEFT, right = align == RIGHT, middle = align == MIDDLE;

        boolean atAsk = false, atBid = false;
        if (calculator != null && vp == calculator.vp && calculator.lastTick != null) {
          var tick = calculator.lastTick;
          if (row.contains(tick.getPrice())) {
            atAsk = tick.isAskTick();
            atBid = !tick.isAskTick();
          }
        }

        switch(getLblType()) {
          case BID_ASK:
            gc.setFont(f.getFont());
            var fc = f.getColor();
            if (fc == null) fc = getFontColor(bg, ctx);
            if (c != null && c.isEnabled()) fc = c.getColor();
            gc.setColor(fc);

            fm = gc.getFontMetrics();
            int xw = fm.stringWidth(baSep);
            int x = area.x + (area.width - xw)/2;
            if (left) x = area.x + maxBAWidth/2 - 10; // Account for the 16 pixels of padding (see calcBAWidth())
            else if (right) x = area.x + area.width - maxBAWidth/2;

            var showTxt = f.isEnabled() && showText(gc);
            if (showTxt) gc.drawString(baSep, x, getCenterY(gc));

            var bidStr = Util.formatMK(row.getBidVolume());
            if (!showZeros && row.getBidVolume() == 0) bidStr = "";
            if (baFiltEnabled && row.getBidVolume() < baFilt) bidStr = "-";
            else setBidImbalance(gc, ctx);

            if (showTxt) {
              if (atBid) gc.setFont(deriveBoldUnderline(gc.getFont()));
              fm = gc.getFontMetrics();
              gc.drawString(bidStr, x - fm.stringWidth(bidStr), getCenterY(gc));
            }

            gc.setFont(f.getFont());
            gc.setColor(fc);
            var askStr = Util.formatMK(row.getAskVolume());
            if (!showZeros && row.getAskVolume() == 0) askStr = "";
            if (baFiltEnabled && row.getAskVolume() < baFilt) askStr = "-";
            else setAskImbalance(gc, ctx);

            if (atAsk) gc.setFont(deriveBoldUnderline(gc.getFont()));

            if (showTxt) gc.drawString(askStr, x + xw, getCenterY(gc));
            break;

          case DELTA:
            gc.setFont(f.getFont());
            if (f.getColor() == null) gc.setColor(getFontColor(bg, ctx));
            else gc.setColor(f.getColor());
            if (c != null && c.isEnabled()) gc.setColor(c.getColor());

            // Highlight bid/ask imbalance (ask imbalance will be displayed if both apply)
            setBidImbalance(gc, ctx);
            setAskImbalance(gc, ctx);

            var str = Util.formatMK(row.getDelta());
            if (Util.isEmpty(str) || (dFiltEnabled && Math.abs(row.getDelta()) < dFilt) || !f.isEnabled() || !showText(gc)) return;
            fm = gc.getFontMetrics();
            int sw = fm.stringWidth(str);
            x = area.x + 3;
            if (right) x = area.x + area.width - sw - 3;
            else if (middle) x = area.x + area.width/2 - sw/2;
            if (isHistogram() && getHistAlign() == MIDDLE && middle) {
              int mid = getMid();
              x = area.x + mid + 3;
              if (row.getDelta() < 0) x  = x - sw - 6;
            }
            gc.drawString(str, x, getCenterY(gc));
            break;

          case VOLUME:
            gc.setFont(f.getFont());
            if (f.getColor() == null) gc.setColor(getFontColor(bg, ctx));
            else gc.setColor(f.getColor());
            if (c != null && c.isEnabled()) gc.setColor(c.getColor());

            // Highlight bid/ask imbalance (ask imbalance will be displayed if both apply)
            setBidImbalance(gc, ctx);
            setAskImbalance(gc, ctx);

            str = Util.formatMK(row.getVolume());
            if (Util.isEmpty(str) || (vFiltEnabled && row.getVolume() < vFilt) || !f.isEnabled() || !showText(gc)) return;
            fm = gc.getFontMetrics();
            x = area.x + 3;
            if (right) x = area.x + area.width - fm.stringWidth(str) - 3;
            else if (middle) x = area.x + area.width/2 - fm.stringWidth(str)/2;
            gc.drawString(str, x, getCenterY(gc));
            break;

          case PROFILE:
            str = Util.formatMK(row.getVolume());
            if (Util.isEmpty(str) || !f.isEnabled()) return;
            gc.setFont(f.getFont());
            if (!showText(gc)) return;
            if (f.getColor() != null) gc.setColor(f.getColor());
            else gc.setColor(getFontColor(gc.getColor(), ctx));
            if (c != null && c.isEnabled()) gc.setColor(c.getColor());
            fm = gc.getFontMetrics();

            sw = fm.stringWidth(str);
            if (left) gc.drawString(str, b.x+2, getCenterY(gc));
            else gc.drawString(str, b.x + b.width - sw-2, getCenterY(gc));
        }
      }

      // Draw support/resistance lines (from Imbalance Settings)
      void drawSRLines(Graphics2D gc, DrawContext ctx)
      {
        if (!isShowImb()) return;
        float close = row.getProfile().getClosePrice();
        float delta = absLevel * tickSize;
        for(var imb : imbSettings) {
          if (imb.supportLine != null && imb.supportLine.isEnabled() && row.isBidImbalance(imb.per, imb.delta, imb.deltaEnabled) &&
                  close >= row.getEndPrice() + delta) {
            BSPP bspp = new BSPP(row.getProfile().getStartTime(),row.getRowPrice(),
                    imb.level);
            if (!SupportBSPPs.contains(bspp)) SupportBSPPs.add(bspp);
            drawSRLine(gc, imb.supportLine, ctx);
            break;
          }
          if (imb.resistanceLine != null && imb.resistanceLine.isEnabled() && row.isAskImbalance(imb.per, imb.delta, imb.deltaEnabled) &&
                  close <= row.getStartPrice() - delta) {
            BSPP bspp = new BSPP(row.getProfile().getStartTime(),row.getRowPrice(),
                    imb.level);
            if (!ResistanceBSPPs.contains(bspp)) ResistanceBSPPs.add(bspp);
            drawSRLine(gc, imb.resistanceLine, ctx);
            break;
          }
        }
      }

      boolean contains(double x, double y) { return area.contains(x, y); }

      // Hack: for BidAsk and Delta bars that have support and resistance lines
      // We want to make sure the lines are displayed even if they are off the screen
      final boolean isAlwaysVisible(DrawContext ctx)
      {
        if (alwaysVisibleSaved != null) return alwaysVisibleSaved;
        var profile = row.getProfile();
        boolean complete = profile.getEndTime() < ctx.getDataContext().getCurrentTime();
        float close = profile.getClosePrice();
        float delta = absLevel * tickSize;
        for(var imb : imbSettings) {
          if ((imb.supportLine != null && imb.supportLine.isEnabled() && row.isBidImbalance(imb.per, imb.delta, imb.deltaEnabled) && close >= row.getEndPrice() + delta) ||
                  (imb.resistanceLine != null && imb.resistanceLine.isEnabled() && row.isAskImbalance(imb.per, imb.delta, imb.deltaEnabled) && close <= row.getStartPrice() - delta)) {
            if (complete) alwaysVisibleSaved = true;
            return true;
          }
        }
        if (complete) alwaysVisibleSaved = false;
        return false;
      }

      int getRightX() { return bar2 == null ? area.x + area.width : bar2.getRightX(); }

      boolean showText(Graphics2D gc) { return area.height - gc.getFontMetrics().getHeight() >= -5; }

      // Override in BidAskBar, VolumeBar, DeltaBar to show the histogram view
      void drawHistogram(Graphics2D gc, DrawContext ctx)
      {
        gc.fillRect(area.x, area.y, Math.max(area.width, 2), area.height-1);
      }

      Color drawFill(Graphics2D gc, DrawContext ctx)
      {
        if (!isShowFill()) return null;
        double delta = row.getDelta();
        Color bg = null;
        if (baLevelsEnabled) {
          if (delta > 0) {
            double per = delta/row.getProfile().getMaxRowDelta();
            if (per <= askPerLow) bg = askColorLow;
            else if (per >= askPerHigh) bg = askColorHigh;
            else bg = askColor;
          }
          else if (delta < 0) {
            double per = delta/row.getProfile().getMinRowDelta();
            if (per <= bidPerLow) bg = bidColorLow;
            else if (per >= bidPerHigh) bg = bidColorHigh;
            else bg = bidColor;
          }
        }
        else bg = adjustFillByDelta(delta > 0 ? askColor : bidColor);
        if (delta == 0) bg = X11Colors.GRAY;
        gc.setColor(bg);
        if (isHistogram()) drawHistogram(gc, ctx);
        else gc.fillRect(area.x, area.y, Math.max(area.width, 2), area.height-1);
        return bg;
      }

      Color adjustFillByDelta(Color fill)
      {
        if (!autoAdjustBa) return fill;
        if (fill == null) return null;
        double delta = row.getDelta();
        if (delta == 0) return fill;

        double mid = row.getProfile().getMaxDelta()/2;
        double factor = (Math.abs(delta) - mid)/mid;
        factor = -1*Util.round(factor/2, 0.001);
        if (factor > 0.7) factor = 0.7;
        if (factor < -0.7) factor = -0.7;
        return Util.adjustColor(fill, factor);
      }

      void setBidImbalance(Graphics2D gc, DrawContext ctx)
      {
        if (!isShowImb()) return;
        for(var s : imbSettings) {
          var f = s.bidFont;
          if (f != null && f.isEnabled() && row.isBidImbalance(s.per, s.delta, s.deltaEnabled)) {
            gc.setFont(f.getFont());
            gc.setColor(f.getColor());
            break;
          }
        }
      }

      void setAskImbalance(Graphics2D gc, DrawContext ctx)
      {
        if (!isShowImb()) return;
        for(var s : imbSettings) {
          var f = s.askFont;
          if (f != null && f.isEnabled() && row.isAskImbalance(s.per, s.delta, s.deltaEnabled)) {
            gc.setFont(f.getFont());
            gc.setColor(f.getColor());
            break;
          }
        }
      }

      void drawOutlines(Graphics2D gc, DrawContext ctx)
      {
        // Highlight the POC (if appropriate)
        if (getPOCBox() != null && getPOCBox().isEnabled() && row.isPOC()) {
          gc.setColor(getPOCBox().getColor());
          gc.setStroke(getPOCBox().getStroke());
          // Should we just outline the entire area for a Profile?
          var outline = isProfile() ? bar : area;
          gc.drawRect(outline.x, outline.y, outline.width-1, outline.height-1);
        }

        // Bid/Ask Absorption Outlines
        if (bidBox == null || askBox == null || (!bidBox.isEnabled() && !askBox.isEnabled()) || !isShowImb()) return;

        float close = row.getProfile().getClosePrice();
        float delta = absLevel * tickSize;
        var outline = area;
        for(var imb : imbSettings) {
          if (bidBox.isEnabled() && row.isBidImbalance(imb.per, imb.delta, imb.deltaEnabled) && close >= row.getEndPrice() + delta) {
            gc.setColor(bidBox.getColor());
            gc.setStroke(bidBox.getStroke());
            gc.drawRect(outline.x, outline.y, outline.width-1, outline.height-1);
            break;
          }
          if (askBox.isEnabled() && row.isAskImbalance(imb.per, imb.delta, imb.deltaEnabled) && close <= row.getStartPrice() - delta) {
            gc.setColor(askBox.getColor());
            gc.setStroke(askBox.getStroke());
            gc.drawRect(outline.x, outline.y, outline.width-1, outline.height-1);
            break;
          }
        }
      }

      void drawTradeDot(Graphics2D gc, FontInfo f, String lbl, int size, int maxSize, int x, int y, int width, int height)
      {
        Shape shape = null;
        switch(dotOutline) {
          case CIRCLE:
            int diameter = Math.max(width, height) + 5;
            double dx = x+width/2 - diameter/2.0, dy = y+height/2 - diameter/2.0-1;
            shape = new Ellipse2D.Double(dx, dy, diameter, diameter);
            break;
          case ROUNDED_RECTANGLE:
            shape = new RoundRectangle2D.Double(x-1, y-1, width+4, height+2, 8, 8);
            break;
          case RECTANGLE:
            shape = new Rectangle(x-1, y-1, width+4, height+2);
            break;
          case AUTO_CIRCLE:
            int d = 5;
            if (maxSize <= 10) {
              d = 15 + Math.round(10*(size/(float)maxSize));
            }
            else {
              d = Math.round(30*(size/(float)maxSize));
            }
            if (d < 5) d = 5;
            gc.setColor(f.getBgColor());
            gc.fill(new Ellipse2D.Double(x+width/2 - d/2.0, y+height/2 - d/2.0 + 1, d, d));
            if (d > width+4) {
              gc.setColor(f.getColor());
              gc.setFont(f.getFont());
              gc.drawString(lbl, x, getCenterY(gc));
            }
            return;
        }

        if (shape != null) {
          gc.setColor(f.getBgColor());
          gc.fill(shape);
        }
        gc.setColor(f.getColor());
        gc.setFont(f.getFont());
        gc.drawString(lbl, x+1, getCenterY(gc));
      }

      int getCenterY(Graphics2D gc) { return area.y + area.height/2 + gc.getFontMetrics().getHeight()/2 - 2; }

      void drawSRLine(Graphics2D gc, PathInfo path, DrawContext ctx)
      {
        if (path == null || !path.isEnabled() || vp.isSummary()) return;
        var _imprints = imprints;
        if (Util.isEmpty(_imprints)) return;
        int ind = getIndex();
        int _lx =-1, _rx=-1;
        int y = area.y + area.height/2;
        _lx = getRightX();
        _rx = (int)graphBounds.getMaxX();
        if (ind < 0) return;
        for(int j = ind+1; j < _imprints.size(); j++) {
          var imp = _imprints.get(j);
          if (row.getStartPrice() > imp.vp.getHighPrice() || row.getEndPrice() <= imp.vp.getLowPrice()) continue;
          if (j == ind+1) {
            _lx = _rx = -1;
            break; // Don't bother drawing the line if it ends at the next bar.
          }
          _rx = isProfile() ? imp.lx : ctx.translateTime(imp.vp.getStartTime());
          break;
        }
        if (_rx < -200) return;
        if (_lx == -1 && _rx == -1) return;

        row.getEndPrice();
        if (ctx.isSelected()) gc.setStroke(path.getSelectedStroke());
        else gc.setStroke(path.getStroke());
        gc.setColor(path.getColor());

        if (lineFont.isEnabled()) {
          float price = row.getStartPrice();
          if (tickInterval > 1) {
            price += tickSize * (tickInterval)/2;
            price = instr.round(price);
          }
          String str = instr.format(price);
          gc.setFont(lineFont.getFont());
          var fm = gc.getFontMetrics();
          int x = _rx - fm.stringWidth(str) - 2;
          gc.drawString(str, x, y + fm.getHeight()/2 - 2);
          _rx = x - 4;
        }
        if (_lx < _rx) gc.drawLine(_lx, y, _rx, y);
      }

      VolumeRow row;
      Rectangle area, bar; // bar is Profile Bar
      // Cache some values here to increase performance
      Boolean alwaysVisibleSaved = null;
      Bar bar2; // second column bar.  This is null if there is no second column or this bar is the second column
    }

    class ProfileBar extends Bar
    {
      ProfileBar(VolumeRow row, Rectangle area, Rectangle bar) { super(row, area); this.bar = bar; }

      @Override String getLblAlign() { return vp.isSummary() ? RIGHT: vpAlign; }
      @Override String getLblType() { return pLblType; }
      @Override FontInfo getLblFont() { return pFont; }
      @Override ColorInfo getLblColor() { return pFontColor; }
      @Override boolean isShowImb() { return vpShowImb; }

      @Override
      boolean contains(double x, double y) { return bar.contains(x, y); }
      @Override
      boolean isProfile() { return true; }

      @Override
      void draw(Graphics2D gc, DrawContext ctx)
      {
        drawSRLines(gc, ctx);

        if (vpSchemeEnabled && Util.compare(vpScheme, VP_SCHEME_BID_ASK)) {
          gc.setColor(vpColor2);
          int aw = (int)((row.getAskVolume() / row.getVolume()) * bar.width);
          int ax = bar.x + bar.width - aw;
          gc.fillRect(ax, bar.y, aw, bar.height-1);
          gc.setColor(vpColor1);
          gc.fillRect(bar.x, bar.y, ax-bar.x, bar.height-1);
        }
        else if (vpSchemeEnabled && Util.compare(vpScheme, VP_SCHEME_DELTA)) {
          gc.setColor(row.getDelta() >= 0 ? vpColor2 : vpColor1);
          gc.fillRect(bar.x, bar.y, bar.width, bar.height-1);
        }
        else if (barColor == null) {
          gc.setColor(eth ? ethBarColor : vpBarColor);
          gc.fillRect(bar.x, bar.y, bar.width, bar.height-1);
        }

        if (barColor != null) {
          gc.setColor(barColor);
          gc.fillRect(bar.x, bar.y, bar.width, bar.height-1);
        }

        drawLabel(gc, ctx, gc.getColor());
        drawOutlines(gc, ctx);
      }

      Color barColor;
    }

    class BidAskBar extends Bar
    {
      BidAskBar(VolumeRow row, Rectangle area) { super(row, area); }

      @Override PathInfo getPOCBox() { return baPocBox; }
      @Override boolean isShowImb() { return baShowImb; }
      @Override boolean isShowFill() { return baShowFill; }
      @Override String getHistAlign() { return baHistAlign; }
      @Override String getLblAlign() { return vp.isSummary() ? RIGHT: baLblAlign; }
      @Override String getLblType() { return baLblType; }
      @Override FontInfo getLblFont() { return baFont; }
      @Override ColorInfo getLblColor() { return baFontColor; }
      @Override boolean isHistogram() { return baHist || vp.isSummary(); }

      @Override int getMid()
      {
        double ask = vp.getMaxAskVolume();
        if (ask <= 0) return area.width;
        double bid = vp.getMaxBidVolume();
        return (int)(bid/(ask + bid) * area.width);
      }

      @Override
      void drawHistogram(Graphics2D gc, DrawContext ctx)
      {
        if (Util.compare(getHistAlign(), LEFT)) {
          double max = vp.getMaxAskVolume() + vp.getMaxBidVolume();
          int w = Math.max((int)((row.getAskVolume() + row.getBidVolume())/max * area.width), 2);
          gc.fillRect(area.x, area.y, w, area.height-1);
        }
        else if (Util.compare(getHistAlign(), RIGHT)) {
          double max = vp.getMaxAskVolume() + vp.getMaxBidVolume();
          int w = Math.max((int)((row.getAskVolume() + row.getBidVolume())/max * area.width), 2);
          gc.fillRect(area.x + area.width - w, area.y, w, area.height-1);
        }
        else {
          int mid = getMid();
          int aw = Math.max((int)(row.getAskVolume()/vp.getMaxAskVolume() * (area.width - mid)), 2);
          int bw = Math.max((int)(row.getBidVolume()/vp.getMaxBidVolume() * mid), 2);
          gc.fillRect(area.x + mid-bw-1, area.y, bw, area.height-1);
          gc.fillRect(area.x + mid+1, area.y, aw, area.height-1);
        }
      }
    }

    class LadderBar extends Bar
    {
      LadderBar(VolumeRow row, Rectangle area) { super(row, area); }

      @Override boolean isShowImb() { return lShowImb; }

      @Override
      void draw(Graphics2D gc, DrawContext ctx)
      {
        drawSRLines(gc, ctx);

        int w = (area.width - getLadderCenterWidth(ctx.getBarWidth()))/2;
        var mv = Math.max(vp.getMaxAskVolume(), vp.getMaxBidVolume());

        if (bidHist != null && bidHist.isEnabled()) {
          gc.setColor(adjLadderColor ? adjustFillByDelta(bidHist.getColor()) : bidHist.getColor());
          double per = row.getBidVolume()/mv;
          int bw = (int)(w*per);
          gc.fillRect(area.x + w - bw, area.y, Math.max(bw,2), area.height-1);
        }
        if (askHist != null && askHist.isEnabled()) {
          gc.setColor(adjLadderColor ? adjustFillByDelta(askHist.getColor()) : askHist.getColor());
          double per = row.getAskVolume()/mv;
          int bw = (int)(w*per);
          gc.fillRect(area.x + area.width - w, area.y, Math.max(bw,2), area.height-1);
        }

        boolean atAsk = false, atBid = false;
        if (calculator != null && vp == calculator.vp && calculator.lastTick != null) {
          var tick = calculator.lastTick;
          if (row.contains(tick.getPrice())) {
            atAsk = tick.isAskTick();
            atBid = !tick.isAskTick();
          }
        }

        var fc = lLblFont.getColor();
        if (fc == null) fc = ctx.getDefaults().getLineColor();
        if (lFontColor != null && lFontColor.isEnabled()) fc = lFontColor.getColor();
        gc.setFont(lLblFont.getFont());
        gc.setColor(fc);

        setBidImbalance(gc, ctx);
        if (atBid) gc.setFont(deriveBoldUnderline(gc.getFont()));
        var str = Util.formatMK(row.getBidVolume());
        var fm = gc.getFontMetrics();
        var showTxt = lLblFont.isEnabled() && showText(gc);
        if (showTxt && (!baFiltEnabled || Math.abs(row.getBidVolume()) > baFilt)) gc.drawString(str, area.x + w/2 - fm.stringWidth(str)/2, getCenterY(gc));

        // Reset font/color
        gc.setFont(lLblFont.getFont());
        gc.setColor(fc);
        setAskImbalance(gc, ctx);
        if (atAsk) gc.setFont(deriveBoldUnderline(gc.getFont()));
        str = Util.formatMK(row.getAskVolume());
        fm = gc.getFontMetrics();
        if (showTxt && (!baFiltEnabled || Math.abs(row.getAskVolume()) > baFilt)) gc.drawString(str, area.x + area.width - w/2 - fm.stringWidth(str)/2, getCenterY(gc));

        // Bid/Ask Absorption Outlines
        if (bidBox == null || askBox == null || (!bidBox.isEnabled() && !askBox.isEnabled()) || !isShowImb()) return;

        float close = row.getProfile().getClosePrice();
        float delta = absLevel * tickSize;
        for(var imb : imbSettings) {
          if (bidBox.isEnabled() && row.isBidImbalance(imb.per, imb.delta, imb.deltaEnabled) && close >= row.getEndPrice() + delta) {
            gc.setColor(bidBox.getColor());
            gc.setStroke(bidBox.getStroke());
            gc.drawRect(area.x, area.y, w, area.height-1);
            break;
          }
          if (askBox.isEnabled() && row.isAskImbalance(imb.per, imb.delta, imb.deltaEnabled) && close <= row.getStartPrice() - delta) {
            gc.setColor(askBox.getColor());
            gc.setStroke(askBox.getStroke());
            gc.drawRect(area.x + area.width - w, area.y, w, area.height-1);
            break;
          }
        }
      }
    }

    class DeltaBar extends Bar
    {
      DeltaBar(VolumeRow row, Rectangle area) { super(row, area);  }

      @Override PathInfo getPOCBox() { return dPocBox; }
      @Override boolean isShowImb() { return dShowImb; }
      @Override boolean isShowFill() { return dShowFill; }
      @Override String getHistAlign() { return dHistAlign; }
      @Override String getLblAlign() { return vp.isSummary() ? RIGHT: dLblAlign; }
      @Override String getLblType() { return dLblType; }
      @Override FontInfo getLblFont() { return dFont; }
      @Override ColorInfo getLblColor() { return dFontColor; }
      @Override boolean isHistogram() { return dHist || vp.isSummary(); }

      @Override int getMid()
      {
        double max = vp.getMaxRowDelta();
        if (max <= 0) return area.width;
        double min = vp.getMinRowDelta();
        if (max > 0 && min < 0) return (int)(Math.abs(min)/(max - min) * area.width);
        return 0;
      }

      // Override in BidAskBar, VolumeBar, DeltaBar to show the histogram view
      @Override
      void drawHistogram(Graphics2D gc, DrawContext ctx)
      {
        if (getHistAlign() == RIGHT || vp.isSummary()) {
          double max = Math.max(vp.getMaxRowDelta(), Math.abs(vp.getMinRowDelta()));
          int w = max == 0 ? 0 : Math.max((int)(Math.abs(row.getDelta())/max * area.width), 2);
          gc.fillRect(area.x + area.width - w, area.y, w, area.height-1);
        }
        else if (getHistAlign() == LEFT) {
          double max = Math.max(Math.abs(vp.getMaxRowDelta()), Math.abs(vp.getMinRowDelta()));
          int w = max == 0 ? 0 : Math.max((int)(Math.abs(row.getDelta())/max * area.width), 2);
          gc.fillRect(area.x, area.y, w, area.height-1);
        }
        else {
          double max = vp.getMaxRowDelta();
          double min = vp.getMinRowDelta();
          double range = max - min;
          int mid = getMid();
          double d = row.getDelta();
          int w = Math.max((int)(Math.abs(d)/range * area.width), 2);
          if (d < 0) gc.fillRect(area.x + mid -w, area.y, w, area.height-1);
          else gc.fillRect(area.x + mid, area.y, w, area.height-1);
        }
      }
    }

    class VolumeBar extends Bar
    {
      VolumeBar(VolumeRow row, Rectangle area) { super(row, area); }

      @Override PathInfo getPOCBox() { return vPocBox; }
      @Override boolean isShowImb() { return vShowImb; }
      @Override boolean isShowFill() { return vShowFill; }
      @Override String getHistAlign() { return vHistAlign; }
      @Override String getLblAlign() { return vp.isSummary() ? RIGHT: vLblAlign; }
      @Override String getLblType() { return vLblType; }
      @Override FontInfo getLblFont() { return vFont; }
      @Override ColorInfo getLblColor() { return vFontColor; }
      @Override boolean isHistogram() { return vHist || vp.isSummary(); }

      @Override
      void drawHistogram(Graphics2D gc, DrawContext ctx)
      {
        int w = Math.max((int)(row.getVolume()/vp.getMaxVolume() * area.width), 2);
        if (Util.compare(getHistAlign(), LEFT)) gc.fillRect(area.x, area.y, w, area.height-1);
        else if (Util.compare(getHistAlign(), RIGHT)) gc.fillRect(area.x + area.width - w, area.y, w, area.height-1);
        else gc.fillRect(area.x + area.width/2 - w/2, area.y, w, area.height-1);
      }
    }
  }

  private class ProfileCalculator implements TickOperation
  {
    ProfileCalculator(int startIndex, DataSeries series, boolean rth)
    {
      this.series = series;
      this.rth= rth;
      ind = startIndex;
      updateNextInterval();
    }

    @Override
    public void onTick(Tick tick)
    {
      if (tick.getTime() < start) return;
      if (tick.getTime() >= end) {
        series.setComplete(ind); // Note: if this is the current unfinished bar, the bar will not be set to complete
        series.setValue(ind, "VP", "VP"); // Dummy value to see if we have a new bar
        if (tick.getTime() >= nextStart) {
          if (!ethSplit) ind++;
          updateNextInterval();
          series.setValue(ind, "VP", "VP"); // Dummy value to see if we have a new bar
        }
        else return; // this tick is in between the end of the bar and the start of the next bar.  Could be RTH data on a daily timeframe
      }
      vp.onTick(tick);
      lastTick = tick;
    }

    void updateNextInterval()
    {
      long s = series.getStartTime(ind);
      long e = series.getEndTime(ind);
      var bs = series.getBarSize();
      if (bs == null) return; // This should not happen

      if (ethSplit) {
        start = Util.getStartOfBar(s, e, instr, bs, true);
        end = Util.getEndOfBar(s, e, instr, bs, true);
        nextStart = end;
        ethSplit = false;
      }
      else if (splitETH) {
        // Split this profile in two
        start = Util.getEndOfBar(s, e, instr, bs, true) - Util.MILLIS_IN_DAY;
        end = Util.getStartOfBar(s, e, instr, bs, true);
        nextStart = end;
        ethSplit = true;
        ethStarts.add(start);
      }
      else {
        start = Util.getStartOfBar(s, e, instr, series.getBarSize(), rth);
        end = Util.getEndOfBar(s, e, instr, series.getBarSize(), rth);
        nextStart = end;
      }

      if (!bs.isFixedSize() && ind >= series.size()-1) end = Long.MAX_VALUE; // for non-linear bars we do not know the end time of the latest bar
      if (vp != null) vp.setComplete(true);
      vp = new VolumeProfile(start, end, instr, tickInterval);
      vp.setFilterTrades(dotsEnabled);
      vp.setMinTradeSize(dotMinSize);
      profileMap.put(start, vp);
    }

    long start, end, nextStart;
    int ind;
    DataSeries series;
    boolean rth, ethSplit=false;
    VolumeProfile vp;
    Tick lastTick;
  }

  public static Font deriveBoldUnderline(Font f)
  {

    Map underlineBold = new HashMap();
    underlineBold.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
    underlineBold.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);

    return f.deriveFont(underlineBold);
  }

}
