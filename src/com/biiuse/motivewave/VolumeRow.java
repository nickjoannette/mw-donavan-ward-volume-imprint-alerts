package com.biiuse.motivewave;

import java.util.Collections;
import java.util.List;

import com.motivewave.platform.sdk.common.Tick;


public class VolumeRow
{
  public static class Trade
  {
    Trade(long time, float price, float size)
    {
      this.time = time;
      this.size = size;
      this.price = price;
    }
    public long getTime() { return time; }
    public float getPrice() { return price; }
    public float getSize() { return size; }
    private long time;
    private float price;
    private float size;
  }

  public VolumeRow(VolumeProfile profile, float startPrice, float endPrice)
  {
    this.profile = profile;
    this.startPrice = startPrice;
    this.endPrice = endPrice;
  }

  public boolean contains(float price)
  {
    return price < endPrice && price >= startPrice;
  }

  public boolean isPOC() { return profile.getPOC() == this; }

  public VolumeProfile getProfile() { return profile; }

  public VolumeRow getNext()
  {
    updateIndex();
    if (index < 0) return null;
    List<VolumeRow> rows = profile.getRows();
    if (index >= rows.size()-1) return null;
    return rows.get(index+1);
  }

  public VolumeRow getPrev()
  {
    updateIndex();
    if (index <= 0) return null;
    return profile.getRows().get(index-1);
  }

  public boolean isBidImbalance(double per, int delta, boolean useDelta)
  {
    if (bidVolume <= 0) return false;
    var next = getNext();
    if (next == null) return false;
    boolean passDelta = useDelta ? bidVolume - next.getAskVolume() >= delta : true;;
    if (next.getAskVolume() == 0) return passDelta;
    if (bidVolume/next.getAskVolume() < per) return false;
    return passDelta;
  }

  public boolean isAskImbalance(double per, int delta, boolean useDelta)
  {
    if (askVolume <= 0) return false;
    var prev = getPrev();
    if (prev == null) return false;
    boolean passDelta = useDelta ? askVolume - prev.getBidVolume() >= delta : true;
    if (prev.getBidVolume() <= 0) return passDelta;
    if (askVolume/prev.getBidVolume() < per) return false;
    return passDelta;
  }

  public float getStartPrice() { return startPrice; }
  public float getEndPrice() { return endPrice; }

  public float getRowPrice()
  {
    if (profile.getRangeTicks() == 1) return startPrice;
    return profile.getInstrument().round((startPrice + endPrice)/2);
  }

  public void addFrom(VolumeRow row)
  {
    this.volume += row.volume;
    this.askVolume += row.askVolume;
    this.bidVolume += row.bidVolume;
  }

  public void onTick(Tick tick)
  {
    // Optimization.  Assume ticks are applied in order.
    // In this case the last TPOCell should be the active one, otherwise a new one needs to be created
    float vol = tick.getVolumeAsFloat();
    if (vol <= 0) vol = 1;
    volume += vol;
    boolean atAsk = tick.isAskTick();
    if (atAsk) askVolume += vol;
    else bidVolume += vol;

    if (profile.isFilterTrades() && vol >= profile.getMinTradeSize()) {
      if (atAsk) {
        if (askTrades == null) askTrades = new ConcurrentList<>();
        askTrades.add(tick);
      }
      else {
        if (bidTrades == null) bidTrades = new ConcurrentList<>();
        bidTrades.add(tick);
      }
    }

    VolumeRow poc = profile.getPOC();
    if (poc == this) return;
    if (poc == null || poc.getVolume() < volume) profile.setPOC(this);
  }

  public double getVolume() { return volume; }
  public double getAskVolume() { return askVolume; }
  public double getBidVolume() { return bidVolume; }
  public double getDelta() { return askVolume - bidVolume; }

  public List<Tick> getAskTrades() { return askTrades == null ? Collections.EMPTY_LIST : askTrades.get(); }
  public List<Tick> getBidTrades() { return bidTrades == null ? Collections.EMPTY_LIST : bidTrades.get(); }

  private void updateIndex()
  {
    var rows = profile.getRows();
    int size = rows.size();
    if (size != lastSize) {
      lastSize = size;
      index = rows.indexOf(this);
    }
  }

  private VolumeProfile profile;
  private float startPrice, endPrice;
  private double volume=0, askVolume=0, bidVolume=0;
  private int lastSize=-1, index=-1; // These are used to resolve the next/prev faster
  private ConcurrentList<Tick> bidTrades;
  private ConcurrentList<Tick> askTrades;
}
