package com.biiuse.motivewave;

import java.util.List;

import com.motivewave.platform.sdk.common.Instrument;

// Builds a summary profile from a list of volume profiles
public class SummaryProfile extends VolumeProfile
{
  public SummaryProfile(long startTime, long endTime, Instrument instr, int rangeTicks)
  {
    super(startTime, endTime, instr, rangeTicks);
  }

  @Override
  public boolean isSummary() { return true; }

  public void populate(List<VolumeProfile> profiles)
  {
    for(var p : profiles) {
      // Note: we need to use start time here (instead of end time) since the profiles could be built using a non-linear bar...
      if (p.getStartTime() < getStartTime() || p.getStartTime() >= getEndTime()) continue;
      for(var r : p.getRows()) {
        var row = findOrCreate(r.getStartPrice());
        row.addFrom(r);
      }
      totalVolume += p.totalVolume;
      totalAskVolume += p.totalAskVolume;
      totalBidVolume += p.totalBidVolume;
      if (openPrice == 0f) {
        openPrice = p.openPrice;
        lowPrice = p.lowPrice;
        highPrice = p.highPrice;
      }
      if (p.lowPrice < lowPrice) lowPrice = p.lowPrice;
      if (p.highPrice > highPrice) highPrice = p.highPrice;
      closePrice = p.closePrice;
      if (p.maxDelta > maxDelta) maxDelta = p.maxDelta;
      if (p.minDelta < minDelta) minDelta = p.minDelta;
    }
    updatePOC();
  }
}
